<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=>'System Admin',
            'username'=>'admin',
            'role_id'=>2,
            'gender'=>'Male',
            'email'=>'admin@gmail.com',
            'password'=>bcrypt('admin')
        ]);
    }
}
