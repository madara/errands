<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->unsigned();
            $table->integer('location_id')->unsigned()->nullable();
            $table->index('location_id');
            $table->index('role_id');
            $table->string('name');
            $table->string('username')->unique();
            $table->string('phone')->unique()->nullable();
            $table->string('email')->unique();
            $table->string('gender');
            $table->string('image')->nullable();
            $table->string('mime')->nullable();
            $table->string('landmark')->nullable();
            $table->string('direction')->nullable();
            $table->string('skill')->nullable();
            $table->string('summary')->nullable();
            $table->string('password');
            $table->tinyInteger('active')->default(1);
            $table->rememberToken();
            $table->foreign('role_id')->references('id')->on('roles')->onDelete('cascade');
            $table->foreign('location_id')->references('id')->on('locations')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
