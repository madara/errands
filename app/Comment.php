<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable=['user_id', 'tasker_id', 'review', 'schedule_id'];
}
