<?php
/**
 * Created by PhpStorm.
 * User: madara
 * Date: 1/14/18
 * Time: 11:10 AM
 */

namespace App\Http\ViewComposers;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
class Payment
{
public function compose(View $view){
    //dd($view);
    $userPayment=\App\Payment::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();
    if($userPayment==!null){
        $subscription_status=$userPayment->subscription_status;
        $approved=$userPayment->status;
        $created=Carbon::parse($userPayment->updated_at);
        $now=Carbon::now();
        $passedDays=$now->diffInDays($created);
        $remainingDays=$userPayment->days-$passedDays;
    }
    elseif($userPayment===null){
        $subscription_status=0;
        $approved=0;
        $remainingDays=0;
    };
 return $view->with(['subscription_status'=> $subscription_status, 'approved'=>$approved, 'remainingDays'=>$remainingDays]);
}
}