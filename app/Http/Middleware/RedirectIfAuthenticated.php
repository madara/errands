<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    { if (Auth::guard($guard)->check()) {
        if(Auth::user()->role->name=='admin'){

            return redirect()->route('admin.index');
        }

        if(Auth::user()->role->name=='user'){
            return redirect()->route('user.index');
        }
        if(Auth::user()->role->name=='tasker'){
            return redirect()->route('tasker.index');
        }

    }
        return $next($request);
    }
}
