<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Profile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->location==null){
            //dd('here');
            //return redirect()->action('TaskerController@profile');

        }
        else{
            //dd('updated');
            return $next($request);
        }

    }
}
