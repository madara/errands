<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class authen
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard ='web')
    { if(!Auth::guard($guard)->check()){
        if(Auth::guard('web')->check()){
            if(Auth::user()->role->name=='user'){
                return redirect()->route('user.index');

            }
            if(Auth::user()->role->name=='tasker'){
                return redirect()->route('tasker.index');
            }
        }
        return view('frontend.login');
    }
        return $next($request);

    }
}
