<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Location;
use App\MessageNotification\SmsMessages;
use App\Schedule;
use App\Skill;
use App\Tasker;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;
use DB;
use Illuminate\Support\Carbon;
use App\Review;
use App\Payment;

class TaskerController extends Controller
{
    public function __construct(){
       // $this->middleware(['profile']);
    }
    public function  index(){
        if(Auth::user()->location==null){
            return redirect()->action('TaskerController@profile')->with('profileMessage', 'You have to update your profile 
            before continuing.');

        }
        $userPayment=Payment::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();
        // dd($userPayment);
        if($userPayment==!null){
            $subscription_status=$userPayment->subscription_status;
        }
        elseif($userPayment===null){
            $subscription_status=1;
        }
        return view('tasker.home', compact(['subscription_status']));
    }
    public function password(){
        if(Auth::user()->location==null){
            return redirect()->action('TaskerController@profile')->with('profileMessage', 'You have to update your profile 
            before continuing.');

        }
        return view('tasker.password');
    }
    public function changePassword(Request $request){
        //dd($request->all());
        //dd('here');
        // $user=Auth::user();
        //$user=findorfail($id);
        $user = User::find(Auth::id());
        //dd($user);
        $this->validate($request,[
            'old_password' => 'required',
            'password'=>'required|min:4',
            'password_confirmation'=>'required|same:password'
        ]);


        $hashedPassword=$user->password;
        //dd($hashedPassword);
        $password=$request->old_password;
        if (Hash::check($request->old_password, $hashedPassword)) {
            //Change the password

            $user->fill([
                'password' => Hash::make($request->password)
            ])->save();
            //$user->passrec=0;
            $user->save();
        }
        else{
            return back()
                ->with('message','The specified password does not match the database password');
        }
        // Alert::success('Password Changed Successfuly', 'Changed!');
        return redirect()->route('tasker.profile');
    }
   public function profile(){
        $user=Auth::user();
        //dd($user->skill->skill);

      $place=$user->location;
       if($place==!null){
           $location=$place->name;
       }
       else{
           $location="";
       }
       $skills=Skill::all();
       $locations=Location::all();
       $query=DB::table('other_skills')->where('user_id', $user->id)->first();
       //dd($query);
      if($query==null){
          $retrieved="";
      }else{
          $others=$query->others;
          $retrieved=explode(',',$others);
      }
        return view('tasker.profilesample', compact(['user', 'locations', 'location', 'skills', 'retrieved']));
   }
    public function updateBasic(Request $request){
//        $user=User::find(Auth::user()->id);
        $this->validate($request,['id_no'=>'required|unique:users']);
        $user=Auth::user();
        $user->update(['name'=>$request->name, 'gender'=>$request->gender, 'id_no'=>$request->id_no]);
        return back();
    }
    public function updateContact(Request $request){
        $user=Auth::user();
        $user->update(['phone'=>$request->phone, 'email'=>$request->email]);
        return back();
    }
    public function updatephoto(Request $request){
        if(Auth::user()->image==null){
            $user=Auth::user();
            // dd(Auth::user());

            $image=$request->image;
            $imagename=$image->getClientOriginalName();
            $mime=$image->getClientMimeType();
            $user->image=$imagename;
            $user->mime=$mime;

            $image->move('profile', $imagename);
             $user->save();
            return back();

        }
        else{
            $filename=Auth::user()->image;
            $path = public_path().'/profile/'.$filename;
            //dd($path);
            unlink($path);
            $user=Auth::user();
            // dd(Auth::user());

            $image=$request->image;
            $imagename=$image->getClientOriginalName();
            $mime=$image->getClientMimeType();
            $user->image=$imagename;
            $user->mime=$mime;

            $image->move('profile', $imagename);

            $user->save();
           // dd('deleted');
            return back();


        }

    }
    public function updatelocation(Request $request){
        //dd($request->all());
        $user=Auth::user();
       $user->update(['location_id'=>$request->location, 'direction'=>$request->direction,
           'landmark'=>$request->landmark]);
        return back();

    }
    public function updatesummary(Request $request){
        //dd($request->all());
        $user=Auth::user();
        $user->update(['summary'=>$request->summary]);

        return back();
    }
    public function updateskills(Request $request){
        $user=Auth::user();
        $id=$user->id;
        if($user->skill_id==null){
          $user->update(['skill_id'=>$request->skill_id]);
        }
        if($user->skill_id==!null){
            $user->update(['skill_id'=>$request->skill_id]);
        }

        //$test= $user->updateOrCreate(['skill_id'=>$skill_id]);

        $request=$request->other_skill;
        if(isset($request)){
            $query=Db::table('other_skills')->where(['user_id'=>$id]);
            $skill_exist=$query->get()->toArray();
            $val=implode(',', $request);
            //dd($val);
           // dd($skill_exist);
            if($skill_exist===[]){
                DB::table('other_skills')->insert(['user_id'=>$user->id,'others'=>$val]);
            }
            else{
                $query->update(['others'=>$val]);
            }
        }
        return back();
    }
    public function viewClients(){

        if(Auth::user()->location==null){
            return redirect()->action('TaskerController@profile')->with('profileMessage', 'You have to update your profile 
            before continuing.');

        }
        $userPayment=Payment::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();
       // dd(['active', $userPayment]);
        $user=Auth::user();
        $clients=$user->tasker->users;
        $userPayment=Payment::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();
        if($userPayment==!null){
            $created=Carbon::parse($userPayment->updated_at);
            $now=Carbon::now();
            $passedDays=$now->diffInDays($created);
            $remainingDays=$userPayment->days-$passedDays;
        }
        else{
            $remainingDays=0;
        }
        return view('tasker.viewClients', compact(['clients','remainingDays']));
    }
    public  function viewClient(Request $request, $id){

        $time=$request->time;
        $date=$request->date;

        $schedule_id=$request->schedule_id;
        $schedule=Schedule::find($schedule_id);
        $tasker_id=Auth::user()->tasker->id;
        $user_id=User::find($id)->id;
        $showPostButton=Comment::where(['tasker_id'=>$tasker_id, 'schedule_id'=>$schedule_id, 'user_id'=>$user_id])
            ->first();
        $query=Schedule::where(['tasker_id'=>$tasker_id, 'user_id'=>$user_id, 'date'=>$date, 'time'=>$time])->first();
        //dd($query);
        $user=User::find($id);
        $reviews=User::find($user_id)->reviews;
       return view('tasker.viewClient', compact(['user', 'time', 'date', 'query', 'showPostButton',
           'schedule_id', 'reviews', 'schedule']));
    }
    public function accepthiring(Request $request, $id){

        $tasker_id=Auth::user()->tasker->id;
        $user_id=User::find($id)->id;
        $clientPhone=User::find($id)->phone;
        $user=Auth::user();
        $date=$request->date;
        $formatedDate=Carbon::parse($date)->format('d-m-Y');
        $time=$request->time;
       // dd([$tasker_id, $user_id,$date,$time]);
        $query=Schedule::where(['tasker_id'=>$tasker_id, 'user_id'=>$user_id, 'date'=>$date, 'time'=>$time])->first();
        $query->update(['status'=>1]);
       // dd($query);
        $messsage=new SmsMessages();
//        $messsage->send($user->name . ' has Accepted your schedule of Date at :
//        '. $formatedDate. ' Time : '.$time,$clientPhone);
       return redirect()->route('tasker.viewclients');

    }
    public function markcompleted(Request $request, $id){
        $tasker_id=Auth::user()->tasker->id;
        $user_id=User::find($id)->id;
        $date=$request->date;
        $time=$request->time;
        $query=Schedule::where(['tasker_id'=>$tasker_id, 'user_id'=>$user_id, 'date'=>$date, 'time'=>$time])->first();
        $query->update(['completed'=>1]);
        return redirect()->route('tasker.viewclients');

    }
    public function rejectHiring(Request $request, $id){
        $user=Auth::user();
        $schedule_id=$request->schedule_id;
        $comment=Comment::where(['schedule_id'=>$schedule_id])->first();
        $tasker_id=Auth::user()->tasker->id;
        $user_id=User::find($id)->id;
        $date=$request->date;
        $time=$request->time;

        $clientPhone=User::find($id)->phone;
        $formatedDate=Carbon::parse($date)->format('d-m-Y');
        $query=Schedule::where(['tasker_id'=>$tasker_id, 'user_id'=>$user_id, 'date'=>$date, 'time'=>$time])->first();
        $query->update(['status'=>2, 'completed'=>null]);
        // dd($query);
        $messsage=new SmsMessages();
        $messsage->send($user->name . ' has rejected your schedule of Date : 
        '. $formatedDate. ' Time : '.$time,$clientPhone);
        if($comment==!null){
            $comment->delete();
        }
        return redirect()->route('tasker.viewclients');

    }
    public  function subscription(){
        if(Auth::user()->location==null){
            return redirect()->action('TaskerController@profile')->with('profileMessage', 'You have to update your profile 
            before continuing.');

        }
        return view('tasker.subscriptions');
    }
    public function savepayment(Request  $request){
        $user=Auth::user();
        $validateReceipt=Payment::where('receipt', $request->receipt)->first();
        if($validateReceipt==!null){
            return back()->with('message', 'The specified MPESA receipt is already in our record,
             please recheck and try again.');
        }
        $userPayment=Payment::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();
        if($userPayment==null){
            $amount=$request->amount;
            $modulo=$amount%100;
            $months=$amount/100;
            //dd($months);
            $days=$months*30;
            $receipt=strtoupper($request->receipt);
            Payment::create(['user_id'=>Auth::user()->id, 'amount'=>$request->amount, 'days'=>$days,
                'receipt'=>$receipt, 'total'=>$request->amount, 'update'=>1, 'subscription_status'=>1]);
        }
        if($userPayment!=null){
//            if ($userPayment->status==0){
//                return back()->with('message', 'Please wait for your previous payment to be approved');
//            }
            $previous_udate_value=$userPayment->update;
            $created=Carbon::parse($userPayment->updated_at);
            $now=Carbon::now();
            $amount=$request->amount;
            $months=$amount/100;
            $days=$months*30;
            $passedDays=$now->diffInDays($created);
            $remainingDays=$userPayment->days-$passedDays;
            $total=$userPayment->total+ $request->amount;
            $updatedDays=$remainingDays+$days;
            $payed=$request->amount;
            $receipt=strtoupper($request->receipt);
            $userPayment->update(['update'=>0]);
            Payment::create(['user_id'=>Auth::user()->id,'amount'=>$payed, 'days'=>$updatedDays ,
                'total'=>$total, 'receipt'=>$receipt, 'status'=>0,'update'=> 1]);

        }

        $messsage=new SmsMessages();
//        $messsage->send($user->name . ' has made payment MPESA Receipt :
//        '. $receipt. ' please access the dashboard to approve this Payment ASAP ','+254717152991');


        return redirect()->route('tasker.viewpayment');

    }
    public function viewpayment(){
        if(Auth::user()->location==null){
            return redirect()->action('TaskerController@profile')->with('profileMessage', 'You have to update your profile 
            before continuing.');

        }
        $user=Auth::user();
        $userPayment=Payment::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first();
        if($userPayment==!null){
            $created=Carbon::parse($userPayment->updated_at);
            $now=Carbon::now();
            $passedDays=$now->diffInDays($created);
            $remainingDays=$userPayment->days-$passedDays;
        }
        else{
            $remainingDays=0;
        }


        $payments=$user->payments;
        return view('tasker.payments', compact(['payments', 'remainingDays']));
    }
    public function paymentExpired(){
        if(Auth::user()->location==null){
            return redirect()->action('TaskerController@profile')->with('profileMessage', 'You have to update your profile 
            before continuing.');
        }
        $message='Your Subscription plan Expired, Please make another payment
        plan to be able to access your clients.';
        return view('tasker.paymentExpired', compact('profileMessage'));
    }
    public function waitApproval(){
        $message='Please Wait for a few minutes for the System Administrator to approve your last payment and this page will refresh Automatically ';
        return view('tasker.waitApproval', compact('message'));
    }

}
