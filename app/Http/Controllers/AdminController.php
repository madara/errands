<?php

namespace App\Http\Controllers;

use App\Location;
use App\Payment;
use App\Skill;
use Illuminate\Http\Request;
use Hash;
Use Auth;
use App\User;
use App\Role;
use Illuminate\Support\Facades\DB;


class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware(['roles', 'authen']);
    }

    public function index(){
        return view('admin.home');
    }
    public function password(){
        return view('admin.password');
    }
    public function changePassword(Request $request){
        //dd($request->all());
        //dd('here');
        // $user=Auth::user();
        //$user=findorfail($id);
        $user = User::find(Auth::id());
        //dd($user);
        $this->validate($request,[
            'old_password' => 'required',
            'password'=>'required|min:4',
            'password_confirmation'=>'required|same:password'
        ]);


        $hashedPassword=$user->password;
        //dd($hashedPassword);
        $password=$request->old_password;
        if (Hash::check($request->old_password, $hashedPassword)) {
            //Change the password

            $user->fill([
                'password' => Hash::make($request->password)
            ])->save();
            //$user->passrec=0;
            $user->save();
        }
        else{
            return back()
                ->with('message','The specified password does not match the database password');
        }
        // Alert::success('Password Changed Successfuly', 'Changed!');
        return redirect()->route('admin.index');
    }
    public function manageUsers(){
        $id=Auth::user()->id;
//        $users=User::join('roles', 'roles.id', 'users.role_id')
//            ->select('users.id', 'users.name as name', 'users.email','users.verified', 'users.username', 'users.active','roles.name as role_name', 'users.created_at')
//            ->where('users.id', '!=', $id)
//            ->get();
        $users=User::all();
       // dd($users);
        $roles=Role::all();

        return view('admin.manageusers', compact(['users', 'roles']));

    }
    public function updateUser(Request $request){
        $user=User::findorfail($request->id);
       // dd($user);
        $user->update(['active'=>$request->active, 'verified'=>$request->verified]);
        return back();

    }
    public function deleteUser($id){
        //dd($id);
        $user=User::findorfail($id);
        $user->delete();

        return back();
    }
    public function approveUser($id){
        $user=User::findorfail($id);
        $user->update(['active'=>1]);
        return back();
    }
    public function disapproveUser($id){

        $user=User::findorfail($id);
        $user->update(['active'=>0]);
        return back();
    }
    public function location(){
        $locations=Location::all();
        return view('admin.location', compact(['locations']));
    }
    public function createLocation(Request $request){
        $location=Location::where('name',$request->name)->first();
        if($location==null){
            Location::create(['name'=>$request->name]);
        }
        else{
            return back()->with('message', 'The specified Location is already in the system');
        }

        return back();

    }
    public function skills(){
        $skills=Skill::all();
        return view('admin.skills', compact(['skills']));
    }
    public function createSkill(Request $request){
        $skill=Skill::where('skill',$request->skill)->first();
        if($skill==null){
            Skill::create(['skill'=>$request->skill]);
        }
        else{
            return back()->with('message', 'The specified Location is already in the system');
        }

        return back();

    }
    public function viewpayments(){
        $payments=Payment::all();
        //dd($payments);
        return view('admin.payments', compact(['payments']));
    }
    public function approvePayment($id){
        //dd('here');
        $payment=Payment::findorfail($id);
        $payment->update(['status'=>1, 'subscription_status'=>1]);
        return back();
    }
    public function disapprovePayment($id){

        //dd($id);
        $payment=Payment::findorfail($id);
        //dd($payment);
        $payment->update(['status'=>0]);
        return back();
    }
    public function editPayment(Request $request){
        //dd('here');
        //dd($request->all());
        $payment=Payment::findorfail($request->id);
        $payment->update(['receipt'=>$request->receipt, 'status'=>$request->status]);
        return back();
    }
    public function manageSubscriptions(){
        $taskers=DB::table('payments')
                    ->join('users', 'users.id', '=', 'payments.user_id')
                    ->select('payments.*', 'users.name')
                    ->where('payments.update',1)
                    ->get();
        //dd($taskers);
        return view('admin.managesubscriptions',compact(['taskers']));
    }

    public function approveSub(Request $request){
        $payment=Payment::findorfail($request->id);
        $payment->update(['subscription_status'=>$request->subscription_status]);
        return back();
    }
    public function approveSubcription($id){
        //dd('here');
        //dd(['id', 'approve']);
        $payment=Payment::findorfail($id);
        $payment->update(['subscription_status'=>1]);
        return back();
    }
    public function disapproveSub($id){

       // dd([$id, 'disaprove']);
        $payment=Payment::findorfail($id);
        //dd($payment);
        $payment->update(['subscription_status'=>0]);
        return back();
    }
    public function deleteLocation($id){
        $location=Location::find($id);
        $location->delete();
        return back();
    }
    public function fetchlocation($id){
        $request = new Request();
        $location = Location::find($id);
        return $location;

    }
    public function updateLocation(Request $request){
        $location=Location::find($request->id);
        $location->update(['name'=>$request->name]);
        return back();

    }
    public function deleteSkill($id){
        $location=Skill::find($id);
        $location->delete();
        return back();
    }
    public function fetchSkill($id){
        $request = new Request();
        $location = Skill::find($id);
        return $location;

    }
    public function updateSkill(Request $request){
        $location=Skill::find($request->id);
        $location->update(['skill'=>$request->skill]);
        return back();

    }

}
