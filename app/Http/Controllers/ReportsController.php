<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\User;

class ReportsController extends Controller
{
    public function users(){
        return view('admin.reports.users.index');
    }
    public function allusers(Request $request){
        $users = User::all();
        $pdf=PDF::loadView('admin.reports.users.all',['users'=>$users]);
        return $pdf->download('all users.pdf');
    }
    public function active(Request $request){
        $users = User::all();
        $pdf=PDF::loadView('admin.reports.users.active',['users'=>$users]);
        return $pdf->download('active users.pdf');
    }
    public function inactive(Request $request){
        $users = User::all();
        $pdf=PDF::loadView('admin.reports.users.deactivated',['users'=>$users]);
        return $pdf->download('inactive users.pdf');
    }
    public function verified(Request $request){
        $users = User::all();
        $pdf=PDF::loadView('admin.reports.users.verified',['users'=>$users]);
        return $pdf->download('verified users accounts.pdf');
    }
    public function unverified(Request $request){
        $users = User::all();
        $pdf=PDF::loadView('admin.reports.users.unverified',['users'=>$users]);
        return $pdf->download('unverified users accounts.pdf');
    }
    public function clients(Request $request){
        $users = User::all();
        $pdf=PDF::loadView('admin.reports.users.clients',['users'=>$users]);
        return $pdf->download('clients.pdf');
    }
    public function taskers(Request $request){
        $users = User::all();
        $pdf=PDF::loadView('admin.reports.users.taskers',['users'=>$users]);
        return $pdf->download('taskers.pdf');
    }
}
