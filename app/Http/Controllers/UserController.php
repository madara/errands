<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Review;
use App\Schedule;
use App\Tasker;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Auth;
use App\User;
use Hash;
use App\Skill;
use App\Location;
use DB;
use App\MessageNotification\SmsMessages;


class UserController extends Controller
{
    public function index(){
        $user=Auth::user();
       $tasks= $user->taskers->count();
       $completed=Schedule::where(['user_id'=>$user->id,'completed'=>2])->count();
       $pending=Schedule::where(['user_id'=>$user->id,'status'=>null])->count();
       $reviews=$user->reviews->count();
        return view('user.home', compact(['tasks', 'completed', 'pending', 'reviews']));
    }
    public function password(){
        return view('user.password');
    }
    public function changePassword(Request $request){
        //dd($request->all());
       // dd('here');
        // $user=Auth::user();
        //$user=findorfail($id);
        $user = User::find(Auth::id());
        //dd($user);
        $this->validate($request,[
            'old_password' => 'required',
            'password'=>'required|min:4',
            'password_confirmation'=>'required|same:password'
        ]);


        $hashedPassword=$user->password;
        //dd($hashedPassword);
        $password=$request->old_password;
        if (Hash::check($request->old_password, $hashedPassword)) {
            //Change the password

            $user->fill([
                'password' => Hash::make($request->password)
            ])->save();
            //$user->passrec=0;
            $user->save();
        }
        else{
            return back()
                ->with('message','The specified password does not match the database password');
        }
        // Alert::success('Password Changed Successfuly', 'Changed!');
        return redirect()->route('user.profile');
    }
    public function profile(){
        $user=Auth::user();

        $place=Location::find($user->location_id);
        if($place==!null){
            $location=$place->name;
            //dd($location);
        }
        else{
            $location="";
        }


        $skills=$user->skills;
        // dd($skills);
        $locations=Location::all();
        return view('user.profile', compact(['user', 'locations', 'location', 'skills']));
    }
    public function updateBasic(Request $request){
//        $user=User::find(Auth::user()->id);
        $user=Auth::user();
        $user->update(['name'=>$request->name, 'gender'=>$request->gender]);
        return back();
    }
    public function updateContact(Request $request){
        $user=Auth::user();
        $user->update(['phone'=>$request->phone, 'email'=>$request->email]);

        return back();
    }
    public function updatephoto(Request $request){
        if(Auth::user()->image==null){
            $user=Auth::user();
            // dd(Auth::user());

            $image=$request->image;
            $imagename=$image->getClientOriginalName();
            $mime=$image->getClientMimeType();
            $user->image=$imagename;
            $user->mime=$mime;

            $image->move('profile', $imagename);

            $user->save();
            return back();

        }
        else{
            //dd('not null');
            $filename=Auth::user()->image;
            $path = public_path().'/profile/'.$filename;
            //dd($path);
            unlink($path);
            $user=Auth::user();
            // dd(Auth::user());

            $image=$request->image;
            $imagename=$image->getClientOriginalName();
            $mime=$image->getClientMimeType();
            $user->image=$imagename;
            $user->mime=$mime;

            $image->move('profile', $imagename);

            $user->save();
            // dd('deleted');
            return back();


        }

    }
    public function updatelocation(Request $request){
        //dd($request->all());
        $user=Auth::user();
        $user->update(['location_id'=>$request->location, 'direction'=>$request->direction, 'landmark'=>$request->landmark]);
        return back();

    }
    public function viewtaskers(){
        $taskers=User::where('role_id', 2)->paginate(3);
        return view('user.viewalltaskers', compact(['taskers']));
    }
    public function findtaskers(){
        $skills=Skill::all();
        $location_id=Auth::user()->location_id;
        $taskers=User::where(['role_id'=>2, 'location_id'=>$location_id])->paginate(3);
        $locations=Location::all();
        return view('user.findtaskers', compact(['skills', 'locations', 'taskers']));
    }
    public function seachTasker(Request $request){
      //dd($request->all());
        $skills=Skill::all();
        $locations=Location::all();
        $taskers= User::where(['role_id'=>2,'location_id'=>$request->location_id, 'skill_id'=>$request->skill_id])->get();

     return view('user.findresults', compact(['taskers','skills', 'locations']));

    }
    public function viewTasker($id){
        //dd($id);
        $tasker=User::find($id);
        $query=DB::table('other_skills')->where('user_id',$id)->first();
       //dd(Tasker::find($tasker->tasker->id)->reviews);
        $reviews=Tasker::find($tasker->tasker->id)->reviews;
      //dd($reviews);
        if($query==null){
            $retrieved="";
        }else{
            $others=$query->others;
            $retrieved=explode(',',$others);
        }


        return view('user.viewtasker', compact(['tasker', 'retrieved', 'reviews']));
    }
    public function hireTasker($id){
       //dd($id);
        $user=Auth::user();
        $tasker=User::find($id)->tasker;
        $locations=Location::all();
        return view('user.hire', compact(['tasker', 'user', 'locations']));

    }
    public function submitSchedule(Request $request){
        //dd($request->all());
        $tasker_id=$request->tasker_id;
       $tasker= User::find($tasker_id)->tasker;
       $phone=$tasker->user->phone;
        $date=$request->date;
        $formated_date=Carbon::parse($date)->format('d-m-Y');
        $user=Auth::user();
        $exists=Schedule::where(['user_id'=>$user->id, 'tasker_id'=>$tasker->id, 'date'=>$date])->get()->toArray();

        if($exists===[]){
           $schedule= Schedule::create(['user_id'=>$user->id,'tasker_id'=>$tasker->id,
                'date'=>$date, 'time'=>$request->time]);
            $message=new SmsMessages();
//            $message->send('You have been booked by  '. $user->name .' From : '.$user->location->name .
//                ', Landmark : '. $user->landmark. ' Direction : '.$user->direction.'
//                ON ' . $formated_date .
//                '  AT '.$schedule->time.' Please login to your dashboard to approve or disapprove this request. ', $phone);

        }else{
            return back()->withErrors('You have already booked the tasker for the selected date, Please 
            try a different date.');
        }
        return redirect()->route('user.mytaskers');


    }
    public function mytaskers(){
        $user=Auth::user();
        $taskers=$user->taskers;
        //dd(Tasker::find(1)->user);
        //dd($taskers);
        return view('user.mytaskers', compact(['taskers']));

    }
    public function approveCompleted(Request $request, $id){
        //dd([$id, $request->all()]);
        $tasker_id=$id;
        $user_id=Auth::user()->id;
        $date=$request->date;
        $time=$request->time;
         //dd([$tasker_id, $user_id,$date,$time]);
        $query=Schedule::where(['tasker_id'=>$tasker_id, 'user_id'=>$user_id, 'date'=>$date, 'time'=>$time])->first();
        //dd($query);
        $query->update(['completed'=>2]);
        // dd($query);
        return back();
    }
    public function viewSchedule(Request $request, $id){
        $tasker=Tasker::find($id)->user;
        $date=$request->date;
        $schedule_id=$request->schedule_id;
        $schedule=Schedule::find($schedule_id);
       // dd($schedule);
        $reviews=Tasker::find($tasker->tasker->id)->reviews;
        //dd($reviews);
        $showPostButton=Review::where(['tasker_id'=>$id, 'schedule_id'=>$schedule_id, 'user_id'=>Auth::user()->id])
                        ->first();
        //dd($showPostButton);
        $time=$request->time;

        return view('user.viewtask', compact(['tasker', 'date', 'time', 'schedule', 'reviews', 'showPostButton']));
    }
    public function markCompleted(Request $request, $id){
        //dd([$id, $request->all()]);
        $user_id=Auth::user()->id;
        $tasker_id=User::find($id)->tasker->id;
        $date=$request->date;
        $time=$request->time;
       // dd([$tasker_id, $user_id,$date,$time]);
        $query=Schedule::where(['tasker_id'=>$tasker_id, 'user_id'=>$user_id, 'date'=>$date, 'time'=>$time])->first();
        //dd($query);
        $query->update(['completed'=>2]);
        // dd($query);
        return back();
    }
    public function cancelSchedule(Request $request, $id){
       $review=Review::where(['schedule_id'=>$id])->first();
        $comment=Comment::where(['schedule_id'=>$id])->first();
        $user=Auth::user();
       $schedule=Schedule::find($id);
       $date=Carbon::parse($schedule->date)->format('d-m-Y');
       $tasker=Tasker::find($schedule->tasker_id)->user;
       $phone=$tasker->phone;
       $schedule->update(['status'=>0, 'completed'=>null]);
       $message=new SmsMessages();
//       $message->send('Your Client '.$user->name .' From :'.$user->location->name.
//           ' canceled the Schedule of Date '.$date .' at Time '.$schedule->time, $phone);
       if($review==!null){
           $review->delete();
       }
        if($comment==!null){
            $comment->delete();
        }
        return back();

    }
    public function reSchedule(Request $request){
        $user=Auth::user();
        $schedule_id=$request->schedule_id;
        $comment=Comment::where(['schedule_id'=>$schedule_id])->first();
        $review=Review::where(['schedule_id'=>$schedule_id])->first();
        $date=$request->date;
        $time=$request->time;
        $formatedRequestDate=Carbon::parse($date)->format('d-m-Y');
        $schedule=Schedule::find($schedule_id);
        $storedDate=Carbon::parse($schedule->date)->format('d-m-Y');
        $storedTime=$schedule->time;
        $schedule->update(['date'=>$date, 'time'=>$time, 'completed'=>null, 'status'=>null]);
        $formatedDate=Carbon::parse($schedule->date)->format('d-m-Y');
        $tasker=Tasker::find($schedule->tasker_id)->user;
        $phone=$tasker->phone;
        $message=new SmsMessages();
//        $message->send('Your Client '.$user->name .' From :'.$user->location->name.
//            ' rescheduled his schedule of Date '.$storedDate .
//            ' and Time '.$storedTime . ' To Date : '.$formatedRequestDate .' , Time : '. $time.
//            ' Login to your dashboard to accept or reject the request', $phone);
        if($review==!null){
            $review->delete();
        }
        if($comment==!null){
            $comment->delete();
        }
       // dd($schedule);

        return back();

    }
    public function getSchedule($id){
        $schedule=Schedule::find($id);
        return $schedule;
    }
}
