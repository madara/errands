<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Auth;
class LoginController extends Controller
{
    //

    use AuthenticatesUsers;
    protected $redirectTo ='/login';
    protected $username= 'username';
    protected $guard ='web';

    public function getlogin(){
        //dd('here');
        if(Auth::guard('web')->check()){
          if(Auth::user()->role->name=='user'){
              return redirect()->route('user.index');

          }
          if(Auth::user()->role->name=='tasker'){
              return redirect()->route('tasker.index');
          }
        }
        return view('frontend.login');
    }

    public function postlogin(Request $request){
       // dd($request->all());
        $auth= Auth::guard('web')->attempt(['username'=>$request->username,'password'=>$request->password]);
        //dd($auth);
        $auth2= Auth::guard('web')->attempt(['username'=>$request->username,'password'=>$request->password, 'active'=>1]);
        //dd($auth2);
        if($auth){
            if($auth2){
                if(Auth::user()->role->name=='user'){
                    //dd('user');
                    return redirect()->route('user.index');
                }
                if(Auth::user()->role->name=='tasker'){
                   if(Auth::user()->location==null){
                       return redirect()->route('tasker.profile');
                   }
                    return redirect()->route('tasker.index');
                }
                if(Auth::user()->role->name=='admin'){
                    return redirect()->route('admin.index');
                }

            }
            else{
                // dd('here');
                return back()->withErrors(['username'=>['Your account has been deactivated, please contact the adminstrator']]);
            }
        }
        return back()->withErrors(['username'=>['Invalid username or Password']]);
    }

    public function getlogout(){
        Auth::guard('web')->logout();
        return redirect()->route('/');
    }
}
