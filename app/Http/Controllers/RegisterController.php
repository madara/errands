<?php

namespace App\Http\Controllers;

use App\Http\Middleware\RedirectIfAuthenticated;
use App\Mail\VerifyMail;
use App\Tasker;
use App\VerifyUser;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Mail;
use PhpParser\Node\Stmt\Return_;

class RegisterController extends Controller
{

    public function register(Request $request){
        $this->validate($request, ['name'=>'required', 'username'=>'required|unique:users', 'email'=>'required|unique:users', 'id_no'=>'unique:users', 'password'=>'required|min:6|confirmed', 'phone'=>'required|unique:users']);

        if($request->role=='user'){
         $user=new User();
         $user->name=$request->name;
         $user->username= $request->username;
         $user->role_id=1;
         $user->active=1;
         $user->email=$request->email;
         $user->phone=$request->phone;
         $user->gender=$request->gender;
         $user->password=bcrypt($request->password);
         $user->save();
         $verifyUser=VerifyUser::create(['user_id'=>$user->id, 'token'=>str_random(40)]);
         $user->sendConfirmAccountNotification($verifyUser->token);
         $status='We sent you an activation code. Check your email and click on the link to verify.';
         return view('frontend.login', compact(['status']));

        }

        if ($request->role=='tasker'){
           // dd('tasker');
            //dd($request->all());
            $user=new User();
            $user->name=$request->name;
            $user->username= $request->username;
            $user->role_id=5;
            $user->id_no=$request->id_no;
            $user->active=1;
            $user->email=$request->email;
            $user->phone=$request->phone;
            $user->gender=$request->gender;
            $user->password=bcrypt($request->password);
            $user->save();
          Tasker::create([
              'user_id'=>$user->id
          ]);
            $verifyUser=VerifyUser::create(['user_id'=>$user->id, 'token'=>str_random(40)]);
            $user->sendConfirmAccountNotification($verifyUser->token);
          return view('frontend.login')->with('status', 'We sent you an activation code. Check your email and click on the link to verify.');
        }
    }
    public function verifyUser($token)
    {
        $verifyUser = VerifyUser::where('token', $token)->first();
        if(isset($verifyUser) ){
            $user = $verifyUser->user;
            if($user->verified==0){
                $user->verified=1;
                $user->save();
                $status='Your e-mail is verified. You can now login.';
            }
            else{
                $status='Your e-mail is already verified. You can now login.';
            }

        }else{
            $warning="Sorry your email cannot be identified.";
            return view('frontend.login', compact(['warning']));
        }
        return view('frontend.login', compact(['status']));
    }

}
