<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
class LoginController extends Controller
{
    use AuthenticatesUsers;
    protected $username= 'username';
    protected $guard ='web';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'email';
    }
    public function redirectPath()
    {

        if(Auth::user()->role->name=='admin'){
            return property_exists($this, 'redirectTo') ? $this->redirectTo : '/administrator';
        }
        elseif(Auth::user()->role->name=='user'){
            return property_exists($this, 'redirectTo') ? $this->redirectTo : '/user';

        }
        elseif(Auth::user()->role->name=='tasker'){
            if(Auth::user()->location==null){
                return property_exists($this, 'redirectTo') ? $this->redirectTo : '/tasker/profile';
            }
            return property_exists($this, 'redirectTo') ? $this->redirectTo : '/tasker';

        }

    }
    public function showLoginForm()
    {
        return view('frontend.login');
    }
     public function login(Request $request)
    {
        $this->validateLogin($request);
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }
        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();

        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath());
    }
    protected function authenticated(Request $request, $user)
    {
        if ($user->verified==0) {
            auth()->logout();
            return back()->with('warning', 'You need to confirm your account. We have sent you an activation code, please check your email.');
        }
        return redirect()->intended($this->redirectPath());
    }
}
