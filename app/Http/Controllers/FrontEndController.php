<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class FrontEndController extends Controller
{
    public function taskers(){
      $taskers=User::where('role_id', 5)->paginate(3);
       // $taskers=User::paginate(3);
        return view('frontend.taskers', compact(['taskers']));
    }
}
