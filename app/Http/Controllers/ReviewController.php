<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReviewController extends Controller
{
   public function saveTaskerReview(Request $request){
       //dd($request->all());
       $tasker_id=$request->tasker_id;
       //dd($tasker_id);
       $schedule_id=$request->schedule_id;
       $user_id=Auth::user()->id;
       Review::create(['tasker_id'=>$tasker_id,'schedule_id'=>$schedule_id,'user_id'=>$user_id, 'review'=>$request->review]);
       return back();

   }
    public function saveUserReview(Request $request){
      // dd($request->all());
        $tasker_id=Auth::user()->tasker->id;
        $user_id=$request->user_id;
        $schedule_id=$request->schedule_id;
        Comment::create(['tasker_id'=>$tasker_id,'schedule_id'=>$schedule_id,'user_id'=>$user_id, 'review'=>$request->review]);
       // dd('saved');
        return back();

    }

}
