<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerifyUser extends Model
{
    protected $fillable=['id', 'user_id', 'token'];
    public $timestamps=true;

    public function user(){
        return $this->belongsTo(User::class);
    }
}
