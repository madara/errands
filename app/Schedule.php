<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable=['user_id', 'tasker_id', 'time', 'date', 'status', 'completed'];
}
