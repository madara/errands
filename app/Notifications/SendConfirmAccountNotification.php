<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SendConfirmAccountNotification extends Notification
{
    use Queueable;

   public $token;
    public function __construct($token)
    {
       $this->token=$token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if($notifiable->role->name=='user'){
            return (new MailMessage)
                ->subject('Confirm Your Account!')
                ->greeting('Hello, '. $notifiable->name )
                ->line('You are receiving this email because we received a registration  request for your account.')
                ->line('Click the button below to activate your account')
                ->action('Confirm Account', route('user.confirm',$this->token))
                ->line('If You did not request a registration, no further action is required!');
        }
        if($notifiable->role->name=='tasker'){
            return (new MailMessage)
                ->subject('Confirm Your Account!')
                ->greeting('Hello, '. $notifiable->name )
                ->line('You are receiving this email because we received a registration request request for your account.')
                ->line('Click the button below to activate your account')
                ->action('Confirm Account', route('tasker.confirm',$this->token))
                ->line('If You did not request a registration, no further action is required!');
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
