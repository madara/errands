<?php

namespace App;

use App\Notifications\SendConfirmAccountNotification;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\UserResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role_id', 'username','phone','location_id', 'role_id', 'skill_id',
        'email', 'image', 'mime', 'location', 'landmark', 'direction', 'active', 'summary','skill',
        'gender', 'id_no', 'verified'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public  function role(){
        return $this->hasOne('App\Role','id','role_id');
    }
    public  function location(){
        return $this->belongsTo('App\Location','location_id','id');
    }

    private function CheckIfUserHasRole($need_role){
        //dd($this->role->name);
        //dd(Auth::user()->email);
        //dd($need_role);
        return (strtolower($need_role)==strtolower($this->role->name))? true : null;
    }
    public function hasRole($roles){
        //dd($roles);
        if (is_array($roles)){
            foreach ($roles as $need_role) {
                if($this->CheckIfUserHasRole($need_role)){
                    //dd($need_role);
                    return true;
                }
            }
        }else
        {
            return $this->CheckIfUserHasRole($roles);
        }
        return false;
    }
    public function skill(){
        return $this->belongsTo(Skill::class);
    }
    public function getShortContentAttribute(){
        return substr($this->summary, 0,random_int(250,270)).'...';

    }
    public function tasker(){
        return $this->hasOne(Tasker::class);
    }

    public function taskers(){
        return $this->belongsToMany(Tasker::class, 'schedules', 'user_id', 'tasker_id')
            ->withPivot(['time', 'date', 'status', 'created_at', 'updated_at', 'completed', 'id'])
            ->orderBy('id', 'desc');
    }
    public function reviews(){
        return $this->belongsToMany(Tasker::class, 'comments', 'user_id', 'tasker_id')
                    ->withPivot(['schedule_id', 'review', 'id', 'created_at'])
                    ->orderBy('id', 'desc');
    }
    public function payments(){
        return $this->hasMany(Payment::class);
    }
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new UserResetPasswordNotification($token));
    }
    public function verify(){
        return $this->hasOne(VerifyUser::class);
    }
    public function sendConfirmAccountNotification($token){
        $this->notify(new SendConfirmAccountNotification($token));
    }
}
