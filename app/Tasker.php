<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tasker extends Model
{

    protected $fillable=['user_id'];
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function users(){
        return $this->belongsToMany(User::class, 'schedules', 'tasker_id', 'user_id')
            ->withPivot(['time', 'date', 'status', 'created_at', 'updated_at', 'completed', 'id']);
    }
    public function reviews(){
        return $this->belongsToMany(User::class, 'reviews', 'tasker_id', 'user_id' )
                    ->withPivot(['schedule_id', 'review','created_at', 'updated_at', 'id'])
                    ->orderBy('id', 'desc');

    }
    public function comments(){
        return $this->belongsToMany(User::class, 'comments', 'user_id', 'tasker_id')
                       ->withPivot(['schedule_id', 'review', 'id', 'created_at', 'updated_at'])
                      ->orderBy('id', 'desc');
    }
}
