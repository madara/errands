<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.index');
})->name('/');
Route::get('/registration', function (){
   return view('frontend.register');
})->name('showRegister');
Route::get('/tasker-register', function (){
    return view('frontend.taskerRegister');
})->name('taskerRegister');

Route::get('/noPermission', function(){
    return view('nopermission');
})->name('nopermission');
Route::get('/taskers', 'FrontEndController@taskers')->name('front.taskers');
Route::post('/registration', 'RegisterController@register')->name('user.register');
Auth::routes();



Route::group(['prefix'=>'user', 'middleware'=> ['auth', 'roles'], 'roles'=>['user']],function (){
    Route::get('/', 'UserController@index')->name('user.index');
    Route::get('/change/password', 'UserController@password')->name('user.password');
    Route::post('save/password', 'UserController@changePassword')->name('user.changePassword');
    Route::get('/profile', 'UserController@profile')->name('user.profile');
    Route::post('/update/basic', 'UserController@updateBasic')->name('user.updateBasic');
    Route::post('/update/contact', 'UserController@updateContact')->name('user.updateContact');
    Route::post('/update/photo', 'UserController@updatephoto')->name('user.updatephoto');
    Route::post('/update/location', 'UserController@updatelocation')->name('user.updatelocation');
    Route::get('/taskers/view', 'UserController@viewtaskers')->name('user.viewtaskers');
    Route::get('/taskers/find', 'UserController@findtaskers')->name('user.findtaskers');
    Route::post('tasker/seach','UserController@seachTasker')->name('user.seachTasker');
    Route::get('tasker/view/{id}','UserController@viewTasker' )->name('user.viewTasker');
    Route::get('/tasker/hire/{id}', 'UserController@hireTasker')->name('user.hireTasker');
    Route::post('/tasker/submit/schedule', 'UserController@submitSchedule')->name('user.submitSchedule');
    Route::get('/tasker/mytasker', 'UserController@mytaskers')->name('user.mytaskers');
    Route::post('/tasker/approveCompleted/{id}', 'UserController@approveCompleted')->name('user.approveCompleted');
    Route::get('/schedule/view/{id}', 'UserController@viewSchedule')->name('user.viewSchedule');
    Route::get('/schedule/markCompleted/{id}', 'UserController@markCompleted')->name('user.markCompleted');
    Route::post('tasker/review', 'ReviewController@saveTaskerReview')->name('user.review');
    Route::post('/schedule/cancel/{id}', 'UserController@cancelSchedule')->name('user.cancelSchedule');
    Route::post('/schedule/reschedule', 'UserController@reSchedule')->name('user.reSchedule');
    Route::get('/reschedule/{id}', 'UserController@getSchedule');


});
Route::group(['prefix'=>'tasker', 'middleware'=> ['auth', 'roles'], 'roles'=>['tasker']],function (){
    Route::get('/', 'TaskerController@index')->name('tasker.index');
    Route::get('/change/password', 'TaskerController@password')->name('tasker.password');
    Route::post('save/password', 'TaskerController@changePassword')->name('tasker.changePassword');
    Route::get('/profile', 'TaskerController@profile')->name('tasker.profile');
    Route::post('/update/basic', 'TaskerController@updateBasic')->name('tasker.updateBasic');
    Route::post('/update/contact', 'TaskerController@updateContact')->name('tasker.updateContact');
    Route::post('/update/photo', 'TaskerController@updatephoto')->name('tasker.updatephoto');
    Route::post('/update/location', 'TaskerController@updatelocation')->name('tasker.updatelocation');
    Route::post('/update/summary', 'TaskerController@updatesummary')->name('tasker.summary');
    Route::post('/update/skills', 'TaskerController@updateskills')->name('tasker.updateskills');
    Route::get('/view/clients', 'TaskerController@viewClients')->name('tasker.viewclients');
    Route::get('/view/client/{id}', 'TaskerController@viewClient')->name('tasker.viewclient');
    Route::post('/client/accepthiring/{id}', 'TaskerController@accepthiring')->name('tasker.acceptbooking');
    Route::post('/client/markcompleted/{id}', 'TaskerController@markcompleted')->name('tasker.markcompleted');
    Route::post('/client/rejectHiring/{id}', 'TaskerController@rejectHiring')->name('tasker.rejectHiring');
    Route::post('user/review', 'ReviewController@saveUserReview')->name('tasker.review');
    Route::get('/subscriptions', 'TaskerController@subscription')->name('tasker.subscription');
    Route::post('payment', 'TaskerController@savepayment')->name('tasker.payment');
    Route::get('payment/view', 'TaskerController@viewpayment')->name('tasker.viewpayment');
    Route::get('payment/expired', 'TaskerController@paymentExpired')->name('tasker.paymentExpired');
    Route::get('payment/wait-admin-approval', 'TaskerController@waitApproval')->name('tasker.waitApproval');

});
Route::group(['prefix'=>'administrator', 'middleware'=>['auth', 'roles'], 'roles'=>['admin']],function (){

    Route::get('/', 'AdminController@manageUsers')->name('admin.index');
    Route::get('/change/password', 'AdminController@password')->name('admin.password');
    Route::post('save/password', 'AdminController@changePassword')->name('admin.changePassword');
    Route::get('/users/manage', 'AdminController@manageUsers' )->name('users.manage');
    Route::post('/users/update', 'AdminController@updateUser')->name('updateUser');
    Route::post('/users/delete/{id}', 'AdminController@deleteUser')->name('deleteUser');
    Route::post('/user/approve/{id}', 'AdminController@approveUser')->name('approveUser');
    Route::post('/user/disapprove/{id}', 'AdminController@disapproveUser')->name('disapproveUser');
    //location
    Route::get('/location', 'AdminController@location')->name('location.add');
    Route::post('/location', 'AdminController@createLocation')->name('location.new');
    Route::post('/location/delete/{id}', 'AdminController@deleteLocation')->name('location.delete');
    Route::post('/location/update', 'AdminController@updateLocation')->name('location.update');
    Route::get('location/fetch/{id}', 'AdminController@fetchlocation');
    //skills
    Route::get('/skills', 'AdminController@skills')->name('skills.add');
    Route::post('/skills', 'AdminController@createSkill')->name('skill.add');
    Route::post('/skills/update', 'AdminController@updateSkill')->name('skill.update');
    Route::post('/skills/delete/{id}', 'AdminController@deleteSkill')->name('skill.delete');
    Route::get('skill/fetch/{id}', 'AdminController@fetchSkill');
    Route::get('/payments', 'AdminController@viewpayments')->name('payments.view');
    Route::post('/approve/payments/{id}', 'AdminController@approvePayment')->name('approve');
    Route::post('/disapprove/payments/{id}', 'AdminController@disapprovePayment')->name('disapprove');
    Route::post('/edit/payments', 'AdminController@editPayment')->name('editPayment');
    Route::get('/subscriptions', 'AdminController@manageSubscriptions')->name('subscription.manage');
    Route::post('/subscriptions/approve', 'AdminController@approveSub')->name('approveSub');
    Route::post('/approve/subscription/{id}', 'AdminController@approveSubcription')->name('sub.approve');
    Route::post('/disapprove/subscription/{id}', 'AdminController@disapproveSub')->name('sub.disapprove');
    //user reports
    Route::get('/reports/users', 'ReportsController@users')->name('admin.reports.users');
    Route::get('/reports/users/generate', 'ReportsController@allusers')->name('admin.reports.users.all');
    Route::get('/reports/users/active', 'ReportsController@active')->name('admin.reports.users.active');
    Route::get('/reports/users/inactive', 'ReportsController@inactive')->name('admin.reports.users.inactive');
    Route::get('/reports/users/clients', 'ReportsController@clients')->name('admin.reports.users.clients');
    Route::get('/reports/users/taskers', 'ReportsController@taskers')->name('admin.reports.users.taskers');
    Route::get('/reports/users/verified', 'ReportsController@verified')->name('admin.reports.users.verified');
    Route::get('/reports/users/unverifeid', 'ReportsController@unverified')->name('admin.reports.users.unverified');

});
##password resets links
Route::get('/password/reset', 'ForgotPasswordController@index')->name('password.resetform');
Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.getemail');
Route::post('/password/reset','ResetPasswordController@reset')->name('password.request');
Route::get('user/password/reset/{token}', 'ResetPasswordController@showResetForm')->name('user.reset');
Route::get('admin/password/reset/{token}', 'ResetPasswordController@showResetForm')->name('admin.reset');
Route::get('tasker/password/reset/{token}', 'ResetPasswordController@showResetForm')->name('tasker.reset');

##account reset links
Route::get('user/account/confirm/{token}', 'RegisterController@verifyUser')->name('user.confirm');
Route::get('tasker/account/confirm/{token}', 'RegisterController@verifyUser')->name('tasker.confirm');
