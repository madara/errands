@extends('admin.master')
@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Taskers Subscriptions <small>Active and Expired Subscriptions</small></h2>
        </div>
        <div class="card-body card-padding">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Date Active</th>
                                <th>Days Remaining</th>
                                <th>Total Amount</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($taskers as $key=> $tasker)
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td>{{$tasker->name}}</td>
                                    <td>{{\Illuminate\Support\Carbon::parse($tasker->created_at)->toDateString()}}</td>
                                    <td>
                                        {{$tasker->days - (\Illuminate\Support\Carbon::now()->diffInDays(\Illuminate\Support\Carbon::parse($tasker->updated_at)))}}
                                    </td>
                                    <td>{{$tasker->total}}</td>
                                    @if($tasker->subscription_status==1)
                                        <td>
                                            <button class="btn btn-success btn-xs" onclick="return disapprove('{{$tasker->id}}','{{$tasker->receipt}}')" ><i class="zmdi zmdi-check-all"></i>
                                                Active
                                            </button>
                                        </td>
                                    @else
                                        <td><button class="btn btn-warning btn-xs " onclick="return approve('{{$tasker->id}}')">Expired</button></td>
                                    @endif
                                    <td>
                                        <button style="color: #FFC107" type="button" onclick="return approveSub('{{$tasker->id}}')" class="btn btn-icon command-edit waves-effect waves-circle edit-btn" ><span class="zmdi zmdi-edit" ></span></button>
                                        <button style="color: #4CAF50" type="button" class="btn btn-icon command-delete waves-effect waves-circle delete-btn"  ><span class="zmdi zmdi-check-all"></span></button></td>
                                    <form action="{{route('sub.approve', $tasker->id)}}" style="visibility: hidden;" id="{{$tasker->id}}" method='POST' >
                                        {{csrf_field()}}

                                    </form>
                                    <form action="{{route('sub.disapprove', $tasker->id)}}" style="visibility: hidden;" id="{{$tasker->receipt}}" method='POST' >
                                        {{csrf_field()}}

                                    </form>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

        </div>
    </div>

    <div class="modal fade" id="approveSub" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Activate or Deactivate Subscription</h4>
                </div>
                <div class="modal-body">
                    <!-- content goes here -->
                    <form action="{{route('approveSub')}}" method="POST">
                        {{csrf_field()}}
                        <input type="hidden" name="id">
                        <div class="form-group">
                            <label for="status">Action</label>
                            <select name="subscription_status" class="form-control selectpicker" required="required">

                                <option value="1" selected="selected">Activate</option>
                                <option value="0">Deactivate</option>
                            </select>
                        </div>



                        <div class="modal-footer">

                            <button type="submit"   class="btn btn-primary btn-hover-green btn-sm pull-left" data-action="save" role="button" >Update</button>

                            <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        function approveSub(id) {
            $("input[name='id']").val(id);
            $('#approveSub').modal('show');
        }
        function approve(id){
            // alert(id);
            document.getElementById(id).submit();
        }
        function disapprove(id, rec){
            //alert(id)
            swal({
                title: "Deactivate Subscription?",
                text: "You are about to make subscription inactive!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, Deactivate!",
                closeOnConfirm: false
            }, function(isConfirm){

                if (isConfirm) {

                    document.getElementById(rec).submit();

                }
            });
        }
    </script>
@endsection
