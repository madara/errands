            <aside id="sidebar" class="sidebar c-overflow">
                <div class="s-profile">
                    <a href="" data-ma-action="profile-menu-toggle">
                        <div class="sp-pic">
                            <img src="{{url('img/profile-pics/1.jpg')}}" alt="">
                        </div>

                        <div class="sp-info">
                            {{Auth::user()->name}}
                            <i class="zmdi zmdi-caret-down"></i>
                        </div>
                    </a>

                    <ul class="main-menu">

                        <li>
                            <a href="{{route('admin.password')}}"><i class="zmdi zmdi-input-antenna"></i> Change Password</a>
                        </li>
                        <li>
                            <a href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <i class="zmdi zmdi-time-restore"></i> Logout</a>
                            <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                                {{csrf_field()}}
                            </form>
                        </li>
                    </ul>
                </div>

                <ul class="main-menu">
                    <li class="active">
                        <a href="{{route('admin.index')}}"><i class="zmdi zmdi-home"></i> Home</a>
                    </li>
                    <li class="sub-menu">
                        <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-accounts-alt"></i> Users</a>

                        <ul>
                            <li><a href="{{route('users.manage')}}">Manage</a></li>
                        </ul>
                    </li>  
                    
                    <li class="sub-menu">
                        <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-pin-drop"></i> Location</a>
                        <ul>
                            <li><a href="{{route('location.add')}}">Add</a></li>
                        </ul>
                        
                    </li>
                    <li class="sub-menu">
                        <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-assignment-account"></i> Skill</a>
                        <ul>
                            <li><a href="{{route('skills.add')}}">Add</a></li>
                        </ul>

                    </li>
                    <li class="sub-menu">
                        <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-money-box"></i> Payments</a>

                        <ul>
                            <li><a href="{{route('payments.view')}}">Approve</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-accounts-list-alt"></i> Subscriptions</a>

                        <ul>
                            <li><a href="{{route('subscription.manage')}}">Manage</a></li>
                            <li><a href="{{route('payments.view')}}">Active</a></li>
                            <li><a href="{{route('payments.view')}}">Expired</a></li>
                        </ul>
                    </li>
                    <li class="sub-menu">
                        <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-attachment"></i> Reports</a>

                        <ul>
                            <li><a href="{{route('admin.reports.users')}}">Users</a></li>
                        </ul>
                    </li>


                </ul>
            </aside>