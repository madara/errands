@extends('admin.master')
@section('content')
    <link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
    <div class="row">
        <div class="card">
            <div class="card-header">
                <h2>Tasker Skills<small>Add skills for the system taskers</small></h2>
                @if(Session::has('message'))
                    <p class="alert alert-danger">{{ Session::get('message') }}</p>
                @endif
            </div>
            <div class="card-body card-padding">
                    {{--<button class="btn btn-success btn-icon-text" id="location"><i class="zmdi zmdi-plus"></i>Add</button>--}}
                     <div class="pull-right"><button class="btn btn-success btn-icon-text" id="location"><i class="zmdi zmdi-plus"></i>Add New Skill</button>
                            <br><br></div>
                     <table id="example" class="display" width="100%" cellspacing="0">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Skill Name</th>
                                <th>Created on</th>
                                <th>Commands</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($skills as $key=>$skill)
                                <tr>
                                    <td>{{++$key}}</td>
                                    <td>{{$skill->skill}}</td>
                                    <td>{{Carbon\Carbon::parse($skill->create_at)->toDayDateTimeString()}}</td>
                                    <td>
                                        <button style="color: #00BCD4" type="button" onclick="return update('{{$skill->id}}')" class="btn btn-icon command-edit waves-effect waves-circle edit-btn" ><span class="zmdi zmdi-edit" ></span></button>
                                        <button style="color: red" type="button" class="btn btn-icon command-delete waves-effect waves-circle delete-btn" onclick="return deleteSkill('{{$skill->id}}')" ><span class="zmdi zmdi-delete"></span></button>
                                        <form action="{{route('skill.delete', $skill->id)}}" style="visibility: hidden;" id="{{$skill->id}}" method='POST' >
                                            {{csrf_field()}}

                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addSkill" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add New Skill</h4>
                </div>
                <div class="modal-body">
                    <!-- content goes here -->
                    <form action="{{route('skills.add')}}" method="POST">
                        {{csrf_field()}}
                        <input type="hidden" name="id">
                        <div class="form-group">
                            <label for="status">Skill</label>
                            <input type="text" name="skill" class="form-control" required id="skill">
                        </div>



                        <div class="modal-footer">

                            <button type="submit"   class="btn btn-primary btn-hover-green btn-sm pull-left" data-action="save" role="button" >Add</button>

                            <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="editSkill" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Update Skill</h4>
                </div>
                <div class="modal-body">
                    <!-- content goes here -->
                    <form action="{{route('skill.update')}}" method="POST">
                        {{csrf_field()}}
                        <input type="hidden" name="id">
                        <div class="form-group">
                            <label for="status">Skill</label>
                            <input type="text" name="skill" class="form-control" required id="skill">
                        </div>



                        <div class="modal-footer">

                            <button type="submit"   class="btn btn-primary btn-hover-green btn-sm pull-left" data-action="save" role="button" >Update</button>

                            <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript">
        $('#location').on('click', function (e) {
            e.preventDefault();
            $('#skill').val(' ')
            $('#addSkill').modal('show')
        });
        $(document).ready(function() {
            $('#example').DataTable( {
                columnDefs: [ {
                    targets: [ 0 ],
                    orderData: [ 0, 1 ]
                }, {
                    targets: [ 1 ],
                    orderData: [ 1, 0 ]
                }, {
                    targets: [ ],
                    orderData: [ 6, 0 ]
                } ]
            } );
        } );
        function deleteSkill(id){
            //alert(id);
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this Skill!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete !",
                closeOnConfirm: false
            }, function(isConfirm){

                if (isConfirm) {

                    document.getElementById(id).submit();

                }
            });
        }
        function update(id){
            var submiturl = "{{URL::to('administrator/skill/fetch')}}";
            $.ajax({
                url:submiturl+ '/'+id,
                type: 'GET',
                data: '',
                success: function(data){
                    console.log(data);
                    $("input[name='skill']").val(data.skill);
                    $("input[name='id']").val(data.id);

                },
                error: function (xhr) {
                    console.log("xhr=" + xhr);
                }
            });
            $('#editSkill').modal('show');
        }
    </script>
@endsection
