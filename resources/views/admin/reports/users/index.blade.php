@extends('admin.master')
@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Clients
                <small>Generate Clients Reports</small>
            </h2>
        </div>
        <div class="card-body card-padding">
            <div class="row">
                <div class="col-md-6">
                    <div class="panel-group" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-collapse">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                       aria-expanded="true" aria-controls="collapseOne">
                                       All System Users
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="collapse in" role="tabpanel"
                                 aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <a href="{{route('admin.reports.users.all')}}"><button class="btn btn-success btn-xs waves-effect">Click to Generate Report</button></a>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-collapse">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#collapse4" href="#collapse4"
                                       aria-expanded="false" aria-controls="collapse4">
                                        All System Clients
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse4" class="collapse" role="tabpanel"
                                 aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <a href="{{route('admin.reports.users.clients')}}"><button class="btn btn-success btn-xs waves-effect">Click to Generate Report</button></a>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-collapse">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a  class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseOne5"
                                       aria-expanded="false" aria-controls="collapseOne5">
                                        All System Taskers
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne5" class="collapse" role="tabpanel"
                                 aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <a href="{{route('admin.reports.users.taskers')}}"><button class="btn btn-success btn-xs waves-effect">Click to Generate Report</button></a>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-collapse">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseTwo6" aria-expanded="false" aria-controls="collapseTwo6">
                                       Active User Accounts
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo6" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <a href="{{route('admin.reports.users.active')}}"><button class="btn btn-success btn-xs waves-effect">Click to Generate Report</button></a>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-collapse">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseThree" aria-expanded="false"
                                       aria-controls="collapseThree">
                                        Deactivated Client Accounts
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="collapse" role="tabpanel"
                                 aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <a href="{{route('admin.reports.users.inactive')}}"><button class="btn btn-success btn-xs waves-effect">Click to Generate Report</button></a>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-collapse">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseFour" aria-expanded="false"
                                       aria-controls="collapseThree">
                                        Verified User Accounts
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour" class="collapse" role="tabpanel"
                                 aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <a href="{{route('admin.reports.users.verified')}}"><button class="btn btn-success btn-xs waves-effect">Click to Generate Report</button></a>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-collapse">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseFour1" aria-expanded="false"
                                       aria-controls="collapseThree">
                                        Unverified User Accounts
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseFour1" class="collapse" role="tabpanel"
                                 aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <a href="{{route('admin.reports.users.unverified')}}"><button class="btn btn-success btn-xs waves-effect">Click to Generate Report</button></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection