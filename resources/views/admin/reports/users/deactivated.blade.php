<!DOCTYPE html>
<html>
<head>
    <title>Users Reports</title>

    <link href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('dataTables.bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/vendors/dataTables/responsive.bootstrap.min.css')}}">

</head>
<style type="text/css">
    hr.style2 {
        border-top: 3px double #8c8b8b !important;
        height: 1px;
    }
</style>
<body>

<div style="text-align: center"><img style="text-align: center" width="125" height="125" src="{{URL::to('img/demo/invoice-logo.png')}}" class="img-thumbnail"></div>
<figcaption><h3 style="text-align: center">Kawindi Errands Information System</h3></figcaption>
<hr class="style2" style="font-size: larger;">
<h2 style="text-align: center">Deactivated Users</h2>
<p style="text-align: right">
    <?php
    date_default_timezone_set('Africa/Nairobi');
    echo " <b style='text-align:right!important;'> Printed On</b> : " . date("Y/m/d") . "<b> At</b> " . date("h:i:sa");
    ?>
</p>
<table class="table table-bordered table-hover toggle-circle default footable-loaded footable">
    <thead>
    <tr>
        <th>#</th>
        <th>Name</th>
        <th>Email</th>
        <th>Role</th>
        <th>Status</th>
        <th>Verified</th>
        <th width="80">Registered On</th>

    </tr>
    </thead>
    <tbody>
    @foreach($users as $key=>$user)
        @if($user->active==0)
            <tr>
                <td>{{++$key}}</td>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>@if($user->role->name=='tasker') Tasker @else Client @endif</td>
                <td>@if($user->active==1) Activated @else Deactivated @endif</td>
                <td>@if($user->verified==1) Verified @else Not Verified @endif</td>
                <td>{{Carbon\Carbon::parse($user->created_at)->format('d-m-Y')}}</td>

            </tr>
        @endif
    @endforeach
    </tbody>


</table>
<!-- jQuery -->
<script src="{{ asset('plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('bootstrap/dist/js/bootstrap.min.js') }}"></script>

</body>
</html>