@extends('admin.master')
@section('content')
<link rel="stylesheet" href="{{ asset('css/jquery.dataTables.min.css') }}">
<div class="row">
    <div class="card">
        <div class="card-header">
         <h2>System Locations<small>Add locations for the operations</small></h2>
            @if(Session::has('message'))
                <p class="alert alert-danger">{{ Session::get('message') }}</p>
            @endif
        </div>
        <div class="card-body card-padding">
            {{--<div class="btn-demo">--}}
                {{--@foreach($locations as $location)--}}
                    {{--<button class="btn btn-default btn-icon-text"><i class="zmdi zmdi-pin-drop"></i>{{$location->name}}</button>--}}
                {{--@endforeach--}}
                {{--<button class="btn btn-success btn-icon-text" id="location"><i class="zmdi zmdi-plus"></i>Add</button>--}}
            {{--</div>--}}
            <div class="pull-right"><button class="btn btn-success btn-icon-text" id="location"><i class="zmdi zmdi-plus"></i>Add New Location</button>
                <br><br></div>
            <table id="example" class="display" width="100%" cellspacing="0">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Location Name</th>
                    <th>Created on</th>
                    <th>Commands </th>
                </tr>
                </thead>
                <tbody>
                @foreach($locations as $key=>$location)
                        <tr>
                            <td>{{++$key}}</td>
                            <td>{{$location->name}}</td>
                            <td>{{Carbon\Carbon::parse($location->create_at)->toDayDateTimeString()}}</td>
                            <td>
                                <button style="color: #00BCD4" type="button" onclick="return update('{{$location->id}}')" class="btn btn-icon command-edit waves-effect waves-circle edit-btn" ><span class="zmdi zmdi-edit" ></span></button>
                                <button style="color: red" type="button" class="btn btn-icon command-delete waves-effect waves-circle delete-btn" onclick="return deleteLocation('{{$location->id}}')" ><span class="zmdi zmdi-delete"></span></button>
                                <form action="{{route('location.delete', $location->id)}}" style="visibility: hidden;" id="{{$location->id}}" method='POST' >
                                    {{csrf_field()}}

                                </form>
                                <form action="{{route('disapproveUser', $location->id)}}" style="visibility: hidden;" id="{{$location->username}}" method='POST' >
                                    {{csrf_field()}}

                                </form>
                            </td>
                        </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="addlocation" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add New Location</h4>
            </div>
            <div class="modal-body">
                <!-- content goes here -->
                <form action="{{route('location.new')}}" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" name="id">
                    <div class="form-group">
                        <label for="status">Location</label>
                        <input type="text" name="name" class="form-control" required id="name">
                    </div>



                    <div class="modal-footer">

                        <button type="submit"   class="btn btn-primary btn-hover-green btn-sm pull-left" data-action="save" role="button" >Add</button>

                        <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>

                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="editlocation" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Update Location</h4>
            </div>
            <div class="modal-body">
                <!-- content goes here -->
                <form action="{{route('location.update')}}" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" name="id">
                    <div class="form-group">
                        <label for="status">Location</label>
                        <input type="hidden" name="id">
                        <input type="text" name="name" class="form-control" required id="name">
                    </div>



                    <div class="modal-footer">

                        <button type="submit"   class="btn btn-primary btn-hover-green btn-sm pull-left" data-action="save" role="button" >Edit</button>

                        <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>

                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="addlocation" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add New Location</h4>
            </div>
            <div class="modal-body">
                <!-- content goes here -->
                <form action="{{route('location.new')}}" method="POST">
                    {{csrf_field()}}
                    <input type="hidden" name="id">
                    <div class="form-group">
                        <label for="status">Location</label>
                        <input type="text" name="name" class="form-control" required id="name">
                    </div>



                    <div class="modal-footer">

                        <button type="submit"   class="btn btn-primary btn-hover-green btn-sm pull-left" data-action="save" role="button" >Add</button>

                        <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>

                    </div>
                </form>
            </div>

        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
    $('#location').on('click', function (e) {
        e.preventDefault();
        $('#name').val(' ')
        $('#addlocation').modal('show')
    });
    $(document).ready(function() {
        $('#example').DataTable( {
            columnDefs: [ {
                targets: [ 0 ],
                orderData: [ 0, 1 ]
            }, {
                targets: [ 1 ],
                orderData: [ 1, 0 ]
            }, {
                targets: [ ],
                orderData: [ 6, 0 ]
            } ]
        } );
    } );
    function deleteLocation(id){
        //alert(id);
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Location!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete !",
            closeOnConfirm: false
        }, function(isConfirm){

            if (isConfirm) {

                document.getElementById(id).submit();

            }
        });
    }
    function update(id){
        var submiturl = "{{URL::to('administrator/location/fetch')}}";
        $.ajax({
            url:submiturl+ '/'+id,
            type: 'GET',
            data: '',
            success: function(data){
                console.log(data);
                $("input[name='name']").val(data.name);
                $("input[name='id']").val(data.id);

            },
            error: function (xhr) {
                console.log("xhr=" + xhr);
            }
        });
        $('#editlocation').modal('show');
    }
</script>
@endsection
