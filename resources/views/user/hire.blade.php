@extends('user.master')
@section('content')
    <style type="text/css">
        #profile-main{
            min-height: 200px;
            max-height: inherit;
        }
    </style>
    <div class="card">
        <div class="card-header">
            <h2>Hire {{$tasker->name}}<small>Put the time and date that your will be available</small></h2>
        </div>
        <div class="card-body card-padding">
            <div class="row">
                @if(count($errors)>0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <form method="POST" action="{{route('user.submitSchedule')}}">
                    {{csrf_field()}}
                    <input type="hidden" name="tasker_id" value="{{$tasker->id}}">
                    <div class="col-sm-5 ">
                        <div class="input-group">
                            <span class="input-group-addon last f-15 ">Date</span>
                            <div class="dtp-container">
                                <input type='text' class="form-control date-picker"
                                       name="date"  placeholder="click here..." required>
                            </div>
                            <span class="input-group-addon last f-20 f-700" id="add_faculty"><i class="zmdi zmdi-calendar"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-5 ">
                        <div class="input-group">
                            <span class="input-group-addon last f-15 ">Time</span>
                            <div class="dtp-container">
                                <input type='text' class="form-control time-picker" name="time"
                                       placeholder="Click here..." required>
                            </div>
                            <span class="input-group-addon last f-20 f-700" id="add_faculty"><i class="zmdi zmdi-time"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-success btn-sm" type="submit" >Proceed</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="clearfix"><br></div>
    </div>
    <div class="card" id="profile-main" >

        <div class="pm-body clearfix">
            <form method="POST" action="{{route('user.updatelocation')}}">
                {{csrf_field()}}
                <div class="pmb-block">
                    <div class="pmbb-header">
                        <h2><i class="zmdi zmdi-my-location m-r-10"></i> Your Location Information</h2>

                        <ul class="actions">
                            <li class="dropdown">
                                <a href="" data-ma-action="profile-edit">
                                    <button class="btn btn-success btn-icon"><i class="zmdi zmdi-edit"></i></button>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="pmbb-body p-l-30">
                        <div class="pmbb-view">
                            <dl class="dl-horizontal">
                                <dt>Location</dt>
                                @if($user->location_id==!null)
                                <dd>{{$user->location->name}}</dd>
                                @endif

                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Landmark</dt>
                                <dd>{{$user->landmark}}</dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt>Direction</dt>
                                <dd>{{$user->direction}}</dd>
                            </dl>

                        </div>

                        <div class="pmbb-edit">
                            <dl class="dl-horizontal">
                                <dt class="p-t-10">Location</dt>
                                <dd>
                                    <div class="fg-line">
                                        <select class=" formcontrol selectpicker" name="location" required>
                                            <option value="" selected>--select--</option>
                                            @foreach($locations as $place)
                                                <option value="{{$place->id}}">{{$place->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt class="p-t-10">Landmark</dt>
                                <dd>
                                    <div class="fg-line">
                                        <input type="text" class="form-control" name="landmark"
                                               value="{{$user->landmark}}">
                                    </div>
                                </dd>
                            </dl>
                            <dl class="dl-horizontal">
                                <dt class="p-t-10">Direction</dt>
                                <dd>
                                    <div class="fg-line">
                                        <textarea class="form-control" name="direction" >{{$user->direction}}</textarea>
                                    </div>
                                </dd>
                            </dl>


                            <div class="m-t-30">
                                <button class="btn btn-primary btn-sm" type="submit">Save</button>
                                <button data-ma-action="profile-edit-cancel" class="btn btn-link btn-sm">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection