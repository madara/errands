@extends('user.master')
@section('content')
    @foreach($taskers as $tasker)
        @if($tasker->location_id!==null && $tasker->skill_id!==null)
            <div class="card w-item">
                <div class="card-header">
                    <div class="media">
                        <div class="pull-left">
                            @if($tasker->image==null)
                            <img class="avatar-img" src="{{URL::to('img/profile-pics/3.jpg')}}" alt="">
                             @else
                                <img  id="pic" class="lgi-img " src="{{URL::to('/profile/'. $tasker->image)}}" alt="">
                            @endif
                        </div>

                        <div class="media-body">
                            <h2>{{$tasker->name}}
                                <small>{{$tasker->skill->skill}} at {{$tasker->location->name}}</small>
                            </h2>
                        </div>
                    </div>
                </div>

                <div class="card-body card-padding">
                    <p>{{$tasker->ShortContent}}</p>

                    <div class="wi-stats clearfix">
                        <div class="wis-numbers">
                            <span><i class="zmdi zmdi-comments"></i> Reviewed {{$tasker->tasker->reviews->count()}} times</span>
                            <span><i class="zmdi zmdi-favorite"></i>  {{DB::table('schedules')->where(['tasker_id'=>$tasker->tasker->id, 'completed'=>2])->count()}} Completed Tasks</span>
                            <span><i class="zmdi zmdi-phone">{{$tasker->phone}}</i></span>
                        </div>
                    </div>
                </div>
                <div class="wi-comments">
                    <!-- Comment form -->
                    <div class="wic-form">
                        <div class=" text-left">
                            <a href="{{route('user.viewTasker',$tasker->id)}}"> <button class="btn btn-sm btn-success waves-effect">Hire</button></a>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endforeach
    <div class="col-md-12 text-center">{{$taskers->links()}}</div>



@endsection