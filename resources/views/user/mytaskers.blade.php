@extends('user.master')
@section('content')
    <div class="row">
        <div class="card">
            <div class="card-header">
                <h2>My Taskers</h2>
                <small>All your hired taskers and the current hired</small>
            </div>
            <div class="card-body card-padding">
                <table class="table table-striped table-bordered table-vmiddle responsive">
                    <thead>
                    <tr>
                        <th>Tasker Name</th>
                        <th>Date Booked</th>
                        <th>Time</th>
                        <th>Hire Status</th>
                        <th>Completed</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($taskers as $tasker)
                        <tr>
                            <td>{{$tasker->user->name}}</td>

                            <td class="center">{{Carbon\Carbon::parse($tasker->pivot->date)->format('d-m-Y')}}</td>

                            <td class="center"> {{$tasker->pivot->time}} </td>
                            @if($tasker->pivot->status===null)
                                <td class="center"><button class="btn btn-warning btn-xs">Pending</button> </td>
                            @elseif($tasker->pivot->status==1)
                                <td class="center"><button class="btn btn-success btn-xs"><i class="zmdi zmdi-check-all"></i>Accepted</button></td>
                            @elseif($tasker->pivot->status==2)
                                <td class="center"><button class="btn btn-danger btn-xs"><i class="zmdi zmdi-block"></i> Rejected</button></td>
                            @elseif($tasker->pivot->status==0)
                                <td class="center"><button class="btn btn-danger btn-xs"><i class="zmdi zmdi-close-circle-o"></i> Canceled</button></td>
                            @endif
                            @if($tasker->pivot->completed==null)
                                <td class="center"><button class="btn btn-warning btn-xs">Pending</button> </td>
                            @elseif($tasker->pivot->completed==2)
                                <td class="center"><button class="btn btn-success btn-xs">
                                        <i class="zmdi zmdi-check-all"></i>
                                        Completed
                                    </button>
                                </td>
                            @elseif($tasker->pivot->completed==1)
                                <td class="center"><button class="btn bgm-lightgreen btn-xs" onclick="return approveCompleted('{{$tasker->pivot->created_at}}')">
                                        <i class="zmdi zmdi-check"></i> Click to Approve
                                    </button>
                                </td>
                            @endif

                            <td class="center">
                                <button style="color: #00BCD4" type="button" onclick="return viewSchedule('{{$tasker->pivot->id}}')"  class="btn btn-icon command-edit waves-effect waves-circle edit-btn" >
                                    <span class="zmdi zmdi-eye" ></span>
                                </button>
                                <button style="color: #00BCD4" type="button" onclick="return reSchedule('{{$tasker->pivot->id}}')" class="btn btn-icon command-edit waves-effect waves-circle edit-btn" >
                                    <span class="zmdi zmdi-edit" ></span>
                                </button>
                                <button style="color: red;" type="button" onclick="return cancelSchedule('{{$tasker->pivot->updated_at}}')" class="btn btn-icon command-edit waves-effect waves-circle edit-btn" >
                                    <span class="zmdi zmdi-close" ></span>
                                </button>

                                <form action="{{route('user.approveCompleted', $tasker->id)}}" style="visibility: hidden;" id="{{$tasker->pivot->created_at}}" method='POST' class="pull-left">
                                    &nbsp&nbsp
                                    {{csrf_field()}}
                                    <input type="hidden" name="date" value="{{$tasker->pivot->date}}">
                                    <input type="hidden" name="time" value="{{$tasker->pivot->time}}">
                                </form>
                                <form action="{{route('user.cancelSchedule', $tasker->pivot->id)}}" style="visibility: hidden;" id="{{$tasker->pivot->updated_at}}" method='POST' class="pull-left">
                                    &nbsp&nbsp
                                    {{csrf_field()}}
                                    <input type="hidden" name="date" value="{{$tasker->pivot->date}}">
                                    <input type="hidden" name="time" value="{{$tasker->pivot->time}}">
                                </form>
                                <form action="{{route('user.viewSchedule', $tasker->pivot->tasker_id)}}" style="visibility: hidden;" id="{{$tasker->pivot->id}}" method='GET' class="pull-left">
                                    &nbsp&nbsp
                                    {{csrf_field()}}
                                    <input type="hidden" name="date" value="{{$tasker->pivot->date}}">
                                    <input type="hidden" name="time" value="{{$tasker->pivot->time}}">
                                    <input type="hidden" name="schedule_id" value="{{$tasker->pivot->id }}">
                                </form>
                                <form action="{{route('user.reSchedule', $tasker->pivot->id)}}" style="visibility: hidden;" id="{{$tasker->pivot->created_at->toDayDateTimeString()}}" method='POST' class="pull-left">
                                    &nbsp&nbsp
                                    {{csrf_field()}}
                                    <input type="hidden" name="date" value="{{$tasker->pivot->date}}">
                                    <input type="hidden" name="time" value="{{$tasker->pivot->time}}">
                                    <input type="hidden" name="schedule_id" value="{{$tasker->pivot->id }}">
                                </form>


                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="reScheduleModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">Reschedule Task</h3>
                </div>
                <div class="modal-body">

                    <!-- content goes here -->
                    <form id="updateform" method="POST" action="{{route('user.reSchedule')}}">
                        {{csrf_field()}}
                        <input type="hidden" name="schedule_id">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label for="date" class="control-lable">Date</label>
                                <input type="text" name="date" class="form-control date-picker" >
                            </div>
                            <div class="form-group col-md-6">
                                <label for="start_date" class="control-lable">Time</label>
                                <input type="text" name="time" class="form-control time-picker">
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified" role="group" aria-label="group button">

                        <div class="btn-group" role="group">
                            <button type="submit"   class="btn btn-primary btn-hover-green" data-action="save" role="button">Update</button>
                        </div>
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script type="text/javascript">
    function approveCompleted($id) {
        document.getElementById($id).submit()

    }
    function viewSchedule($id) {
        document.getElementById($id).submit()

    }
    function cancelSchedule($id) {
        swal({
            title: "Cancel Schedule?",
            text: "You will have to  reschedule the task!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, Cancel!",
            closeOnConfirm: false
        }, function(isConfirm){

            if (isConfirm) {
                document.getElementById($id).submit()
            }
        });

    }
    function reSchedule(id) {
        var editurl="{{ URL::to('user/reschedule')}}";
        $.ajax({
            url:editurl+'/'+id,
            type:'GET',
            data:'',
            success:function (data) {
                console.log(data)
                var date= moment(data.date).format('YYYY-MM-DD');
                $("input[name='date']").val(date);
                $("input[name='time']").val(data.time);
                
            },
            error:function (xhr) {
                console.log("xhr=" + xhr);
                
            }

        })
        $("input[name='schedule_id']").val(id)
        $('#reScheduleModal').modal('show')
         //document.getElementById(id).submit()
    }
</script>
@endsection