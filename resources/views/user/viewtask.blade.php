@extends('user.master')
@section('content')
    <style type="text/css">
        .pmbb-header{
            margin-bottom:25px;
            position:relative;

        }
        .pmbb-header h2{
            margin:0;
            font-weight:100;
            font-size:20px;
        }
        #f-menu{
            display: inline-block;

            padding-left: 0;
            margin-left: -5px;
            margin-top: 5px;
            list-style: none;
        }
        #f-menu li{
            display: inline-block;
            padding-left: 5px;
            padding-right: 5px;

        }
    </style>

    <div class="timeline">
        <div class="t-view" data-tv-type="text">
            <div class="tv-header media">
                <a href="" class="tvh-user pull-left">
                    <img class="img-responsive" src="{{URL::to('profile/'. $tasker->image)}}" alt="">
                </a>
                <div class="media-body p-t-5">
                    <strong class="d-block">{{$tasker->name}}</strong>
                    <small class="c-gray">{{$tasker->skill->skill .' at '. $tasker->location->name}}</small>
                </div>
            </div>
            <div class="tv-body">

                <div class="clearfix"></div>
                <div class="pm-body clearfix">
                    <div class="pmb-block ">
                        <div class="pmbb-header">
                            <h2><i class="zmdi zmdi-calendar m-r-10"></i> Schedule</h2>
                        </div>
                        <div class="pmbb-body p-l-30">

                            <div class="pmbb-view">
                                <dl class="dl-horizontal">
                                    <dt>Date</dt>
                                    <dd>{{$date}}</dd>
                                </dl>

                                <dl class="dl-horizontal">
                                    <dt>Time</dt>
                                    <dd>{{$time}}</dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                    <div class="pmb-block ">
                        <div class="pmbb-header">
                            <h2><i class="zmdi zmdi-trending-up m-r-10"></i> Task Status</h2>
                        </div>
                        <div class="pmbb-body p-l-30">

                            <div class="pmbb-view">
                                <dl class="dl-horizontal">
                                    <dt>Approved</dt>
                                    @if($schedule->status==1)
                                        <dd><button class="btn btn-success btn-xs"><i class="zmdi zmdi-check-all"></i> Yes</button></dd>
                                    @elseif($schedule->status==null)
                                        <dd><button class="btn btn-warning btn-xs">Pending</button></dd>
                                    @elseif($schedule->status==2)
                                        <dd><button class="btn btn-danger btn-xs"><i class="zmdi zmdi-block"></i> No</button></dd>
                                    @endif
                                </dl>

                                <dl class="dl-horizontal">
                                    <dt>Completed</dt>
                                    @if($schedule->completed==null)
                                        <dd><button class="btn btn-warning btn-xs">Pending</button></dd>
                                    @elseif($schedule->completed==2)
                                        <dd><button class="btn btn-success btn-xs"><i class="zmdi zmdi-check-all"></i>Yes</button></dd>
                                    @elseif($schedule->completed==1)
                                        <dd><button class="btn bgm-lightgreen btn-xs" onclick="return approveCompleted('{{$tasker->id}}')"><i class="zmdi zmdi-check"></i> Click to Approve</button></dd>
                                    @endif

                                </dl>

                                <form action="{{route('user.markCompleted', $tasker->id)}}" style="visibility: hidden;" id="{{$tasker->id}}" method='GET' class="pull-left">
                                    &nbsp&nbsp
                                    {{csrf_field()}}
                                    <input type="hidden" name="date" value="{{$date}}">
                                    <input type="hidden" name="time" value="{{$time}}">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <ul class="tvb-stats">
                    <li class="tvbs-comments">
                        {{DB::table('schedules')
                        ->where(['tasker_id'=>$tasker->tasker->id, 'completed'=>2])
                        ->count()}} Completed Tasks
                    </li>
                    <li class="tvbs-likes">{{$tasker->tasker->reviews->count()}} Reviews</li>
                </ul>

                <a class="tvc-more" href=""><i class="zmdi zmdi-mode-comment"></i> View all 54
                    Comments</a>
            </div>

            <div class="tv-comments">
                <ul class="tvc-lists">
                    @forelse($reviews->sortBy('pivot_created_at') as $review)
                    <li class="media">
                        <a href="" class="tvh-user pull-left">
                            <img class="img-responsive" src="{{URL::to('profile/'.$review->image)}}" alt="">
                        </a>
                        <div class="media-body">
                            <strong class="d-block">{{$review->name}}</strong>
                            <small class="c-gray">{{$review->pivot->created_at->toDayDateTimeString()}}</small>

                            <div class="m-t-10">{{$review->pivot->review}}
                            </div>

                        </div>
                    </li>
                    @empty
                        <li class="media">
                            <div class="media-body">
                                <div class="m-t-10" style="color: #dd7b0b">No reviews for {{$tasker->name}} at the moment</div>
                            </div>
                        </li>
                    @endforelse
                    @if($showPostButton==null)
                        @if($schedule->status==1 && $schedule->completed==1 || $schedule->completed==2)
                            <form action="{{route('user.review')}}" method="POST">
                                {{csrf_field()}}
                            <li class="p-20">
                                <div class="fg-line">
                                    <input type="hidden" name="tasker_id" value="{{$tasker->tasker->id}}">
                                    <input type="hidden" name="schedule_id" value="{{$schedule->id}}">
                                    <textarea class="form-control auto-size" placeholder="Write a review..." name="review"></textarea>
                                </div>

                                <button class="m-t-15 btn btn-primary btn-sm" type="submit">Post</button>
                            </li>
                            </form>
                        @endif
                    @endif

                </ul>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="text-center">
            <a href="#">
                <button type="submit" class="btn btn-primary  btn-sm">
                    Proceed to Payment
                </button>
            </a>
        </div>


    </div>

@endsection
@section('scripts')
<script type="text/javascript">
    function approveCompleted($id) {
        //alert($id)
        document.getElementById($id).submit()

    }
</script>

@endsection