@extends('user.master')
@section('content')
    <style type="text/css">
        .pmbb-header{
            margin-bottom:25px;
            position:relative;

        }
        .pmbb-header h2{
            margin:0;
            font-weight:100;
            font-size:20px;
        }
        #f-menu{
            display: inline-block;

            padding-left: 0;
            margin-left: -5px;
            margin-top: 5px;
            list-style: none;
        }
        #f-menu li{
            display: inline-block;
            padding-left: 5px;
            padding-right: 5px;

        }
    </style>

    <div class="timeline">
        <div class="t-view" data-tv-type="text">
            <div class="tv-header media">
                <a href="" class="tvh-user pull-left">
                    <img class="img-responsive" src="{{URL::to('profile/'. $tasker->image)}}" alt="">
                </a>
                <div class="media-body p-t-5">
                    <strong class="d-block">{{$tasker->name}}</strong>
                    <small class="c-gray">{{$tasker->skill->skill .' at '. $tasker->location->name}}</small>
                </div>
            </div>
            @if(count($errors)>0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="tv-body">
                <p>{{$tasker->summary}}</p>

                <div class="clearfix"></div>
               <div class="pm-body clearfix">
                   <div class="pmb-block ">
                       <div class="pmbb-header">
                           <h2><i class="zmdi zmdi-assignment-account m-r-10"></i> Skills</h2>
                       </div>
                       <div class="pmbb-body p-l-30">

                           <div class="pmbb-view">
                               <dl class="dl-horizontal">
                                   <dt>Main</dt>
                                   <dd>{{$tasker->skill->skill}}</dd>
                               </dl>

                               <dl class="dl-horizontal">
                                   <dt>Others</dt>
                                   @if($retrieved==!"")
                                       @foreach($retrieved as $retrieve)
                                           <ul id="f-menu">
                                               <li>{{App\Skill::where('id',$retrieve)->first()->skill}}</li>
                                           </ul>
                                       @endforeach
                                   @endif
                               </dl>
                           </div>
                       </div>
                   </div>
                   <div class="pmb-block ">
                       <div class="pmbb-header">
                           <h2><i class="zmdi zmdi-pin-account m-r-10"></i>Contact Information</h2>
                       </div>
                       <div class="pmbb-body p-l-30">

                           <div class="pmbb-view">
                               <dl class="dl-horizontal">
                                   <dt>Mobile Phone</dt>
                                   <dd>{{$tasker->phone}}</dd>
                               </dl>
                               <dl class="dl-horizontal">
                                   <dt>Email Address</dt>
                                   <dd>{{$tasker->email}}</dd>
                               </dl>
                               <dl class="dl-horizontal">
                                   <dt>Location</dt>
                                   <dd>{{$tasker->location->name}}</dd>
                               </dl>
                               <dl class="dl-horizontal">
                                   <dt>Direction</dt>
                                   <dd>{{$tasker->direction}}</dd>
                               </dl>
                               <dl class="dl-horizontal">
                                   <dt>Landmark</dt>
                                   <dd>{{$tasker->landmark}}</dd>
                               </dl>
                           </div>
                       </div>
                   </div>
               </div>

                <ul class="tvb-stats">
                    <li class="tvbs-comments">
                        {{DB::table('schedules')->where(['tasker_id'=>$tasker->tasker->id, 'completed'=>2])->count() }} Completed Tasks
                    </li>
                    <li class="tvbs-likes">{{$tasker->tasker->reviews->count()}} Reviews</li>
                </ul>

                <a class="tvc-more" href=""><i class="zmdi zmdi-mode-comment"></i> View all 54
                    Reviews</a>
            </div>

            <div class="tv-comments">
                <ul class="tvc-lists">
                    @forelse($reviews ->sortByDesc('pivot_id') as $review)
                        <li class="media">
                            <a href="" class="tvh-user pull-left">
                                <img class="img-responsive" src="{{URL::to('profile/'.$review->image)}}" alt="">
                            </a>
                            <div class="media-body">
                                <strong class="d-block">{{$review->name}}</strong>
                                <small class="c-gray">{{$review->pivot->created_at->toDayDateTimeString()}}</small>

                                <div class="m-t-10">{{$review->pivot->review}}
                                </div>

                            </div>
                        </li>
                    @empty
                        <li class="media">
                            <div class="media-body">
                                <div class="m-t-10" style="color: #dd7b0b">No reviews for {{$tasker->name}} at the moment</div>
                            </div>
                        </li>
                    @endforelse
                </ul>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="row">

            <form method="POST" action="{{route('user.submitSchedule')}}">
                {{csrf_field()}}
                <input type="hidden" name="tasker_id" value="{{$tasker->id}}">
                <div class="col-sm-5 ">
                    <div class="input-group">
                        <span class="input-group-addon last f-15 ">Date</span>
                        <div class="dtp-container">
                            <input type='text' class="form-control date-picker"
                                   name="date"  placeholder="click here..." required>
                        </div>
                        <span class="input-group-addon last f-20 f-700" id="add_faculty"><i class="zmdi zmdi-calendar"></i></span>
                    </div>
                </div>
                <div class="col-sm-5 ">
                    <div class="input-group">
                        <span class="input-group-addon last f-15 ">Time</span>
                        <div class="dtp-container">
                            <input type='text' class="form-control time-picker" name="time"
                                   placeholder="Click here..." required>
                        </div>
                        <span class="input-group-addon last f-20 f-700" id="add_faculty"><i class="zmdi zmdi-time"></i></span>
                    </div>
                </div>
                <div class="col-sm-2">
                    <button class="btn btn-success btn-sm" type="submit" >Proceed</button>
                </div>
            </form>
        </div>
        {{--<div class="text-center"><a href="{{route('user.hireTasker', $tasker->id)}}"><button type="submit" class="btn btn-primary  btn-sm">Hire {{$tasker->name}}</button></a> </div>--}}


    </div>

@endsection