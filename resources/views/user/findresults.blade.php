@extends('user.master')
@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Find Taskers<small>Find Taskers by looking for their skills and location</small></h2>
        </div>
        <div class="card-body card-padding">
            <div class="row">
                <form method="POST" action="{{route('user.seachTasker')}}">
                    {{csrf_field()}}
                    <div class="col-sm-5 ">
                        <div class="input-group">
                            <span class="input-group-addon last f-15 ">Location</span>
                            <select class="form-control selectpicker" name="location_id" id="faculty_id" required>
                                <option value="" selected>--------------Your Location-------------</option>
                                @foreach($locations as $location)
                                    <option value="{{$location->id}}">{{$location->name}}</option>
                                @endforeach
                            </select>
                            <span class="input-group-addon last f-20 f-700" id="add_faculty"><i class="zmdi zmdi-plus-square"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-5 ">
                        <div class="input-group">
                            <span class="input-group-addon last f-15 ">Skill</span>
                            <select class="form-control selectpicker" name="skill_id" id="faculty_id" required>
                                <option value="" selected>-----------What Skill-------------</option>
                                @foreach($skills as $skill)
                                    <option value="{{$skill->id}}">{{$skill->skill}}</option>
                                @endforeach

                            </select>
                            <span class="input-group-addon last f-20 f-700" id="add_faculty"><i class="zmdi zmdi-plus-square"></i></span>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-success btn-sm" type="submit" >Proceed</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
@forelse($taskers as $tasker)
    <div class="row col-md-4  m-l-5">
        <div class="card profile-view">
            <div class="pv-header">
                <img src="{{URL::to('/profile/'. $tasker->image)}}" class="pv-main" alt="">
            </div>

            <div class="pv-body">
                <h2>{{$tasker->name}}</h2>
                <small>{{$tasker->ShortContent}}
                </small>

                <ul class="pv-contact">
                    <li><i class="zmdi zmdi-pin"></i> {{$tasker->location->name}}</li>
                    <li><i class="zmdi zmdi-phone"></i> {{$tasker->phone}}</li>
                </ul>

                <ul class="pv-follow">
                    <li>Skill</li>

                    <li>{{$tasker->skill->skill}}</li>
                </ul>
                <div class="card rating-list">
                    <div class="card-header text-center">
                        <h2>Rated 25 times</h2>
                        <div class="rl-star">
                            <i class="zmdi zmdi-star active"></i>
                            <i class="zmdi zmdi-star active"></i>
                            <i class="zmdi zmdi-star active"></i>
                            <i class="zmdi zmdi-star"></i>
                            <i class="zmdi zmdi-star"></i>
                        </div>
                        <ul class="pv-follow">
                            <li>20 Tasks</li>
                            <li>51 Likes</li>
                        </ul>
                    </div>
                </div>

                <a href="{{route('user.viewTasker',$tasker->id)}}" class="pv-follow-btn">View</a>
            </div>
        </div>
    </div>
@empty
    <div class="card warning">
        <div class="card-header">
         <h2>OOPs <small class="f-20" style="color: #ff870a;">No taskers found at your location, please try searching with a different criteria</small></h2>
        </div>
    </div>
@endforelse
@endsection