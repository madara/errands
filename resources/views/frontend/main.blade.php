<!DOCTYPE html>
<html lang="en">
<head>
    <title>Kawindi Errands Information System</title>
    <!-- meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //meta tags -->
    <!-- custom Theme files -->
    <link href="{{URL::to('css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{URL::to('css/style2.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{URL::to('css/chocolat.css')}}" rel="stylesheet">

    <link href="{{URL::to('vendors/bower_components/animate.css/animate.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/sweetalert/dist/sweetalert.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css')}}" rel="stylesheet">
    {{--<link href="{{URL::to('css/app_1.min.css')}}" rel="stylesheet">--}}
    <link href="{{URL::to('css/profileview.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/summernote/dist/summernote.css')}}" rel="stylesheet">
    <link href="{{URL::to('summernote/dist/summernote.css')}}" rel="stylesheet">
    <script src="{{URL::to('js/jquery-1.11.1.min.js')}}"></script>
    <!-- //js -->

</head>
<body>
<!-- header -->
@include('frontend.includes.header')
@include('frontend.includes.navigation')
{{--@include('frontend.includes.banner')--}}
<div class="container-fluid" style="background-color: #F7F7F7">
    @yield('content')
</div>
<div class="row">
    @include('frontend.includes.footer')
</div>

<!-- //header -->

<script src="{{URL::to('')}}"></script>
<!-- start-smooth-scrolling -->
<script type="text/javascript" src="{{URL::to('js/move-top.js')}}"></script>
<script type="text/javascript" src="{{URL::to('js/easing.js')}}"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            // event.preventDefault();

            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- //end-smooth-scrolling -->
<!-- smooth-scrolling-of-move-up -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
         var defaults = {
         containerID: 'toTop', // fading element id
         containerHoverID: 'toTopHover', // fading element hover id
         scrollSpeed: 1200,
         easingType: 'linear'
         };
         */

        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{URL::to('js/bootstrap.js')}}"></script>
</body>
</html>