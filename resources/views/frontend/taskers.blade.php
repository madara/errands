@extends('frontend.main')
@section('content')
    @foreach($taskers as $tasker)
            @if($tasker->location_id!==null && $tasker->skill_id!==null)
        <div class="row col-md-4  m-l-5">
            <div class="card profile-view">
                <div class="pv-header">
                    <img src="{{URL::to('/profile/'. $tasker->image)}}" class="pv-main" alt="">
                </div>

                <div class="pv-body">
                    <h2>{{$tasker->name}}</h2>
                    <small>{{$tasker->ShortContent}}
                    </small>

                    <ul class="pv-contact">
                        @if($tasker->location_id==!null)
                            <li><i class="zmdi zmdi-pin"></i> {{$tasker->location->name}}</li>
                        @endif
                        <li><i class="zmdi zmdi-phone"></i> {{$tasker->phone}}</li>
                    </ul>

                    <ul class="pv-follow">
                        <li>Skill</li>
                        @if($tasker->skill_id==!null)
                            <li>{{$tasker->skill->skill}}</li>
                        @endif
                    </ul>
                        <div class="card-header text-center">
                            <h2>Rated {{$tasker->tasker->reviews->count()}} times</h2>
                            <ul class="pv-follow">
                                <li>
                                    {{DB::table('schedules')->where(['tasker_id'=>$tasker->tasker->id, 'completed'=>2])->count()}}
                                    Completed Tasks
                                </li>
                            </ul>
                        </div>

                    <a href="{{route('user.viewTasker',$tasker->id)}}" class="pv-follow-btn">Hire</a>
                </div>
            </div>
        </div>
       @endif
    @endforeach
    <div class="col-md-12 text-center">{{$taskers->links()}}</div>

@endsection