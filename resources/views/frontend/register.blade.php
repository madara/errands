<!DOCTYPE html>
<html lang="en">
<head>
    <title>Kawindi Errands Information System</title>
    <!-- meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //meta tags -->
    <!-- custom Theme files -->
    <link href="{{URL::to('css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{URL::to('css/style2.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{URL::to('css/chocolat.css')}}" rel="stylesheet">

    <link href="{{URL::to('vendors/bower_components/animate.css/animate.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/sweetalert/dist/sweetalert.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css')}}" rel="stylesheet">

    <link href="{{URL::to('css/app_2.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('css/profileview.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/summernote/dist/summernote.css')}}" rel="stylesheet">
    <link href="{{URL::to('summernote/dist/summernote.css')}}" rel="stylesheet">
    <script src="{{URL::to('js/jquery-1.11.1.min.js')}}"></script>

    <!-- //js -->

</head>
<body >
<!-- header -->
@include('frontend.includes.header')
@include('frontend.includes.navigation')

{{--@include('frontend.includes.banner')--}}
<div class="container-fluid"style="background-color: #F7F7F7">
    <br><br><br>

    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <form class="form-horizontal " role="form" method="POST" action="{{route('user.register')}}">
                {{csrf_field()}}
                <div class="card-header">
                    <h1>Register
                    </h1>
                </div>
                @if (!empty($status))
                    <div class="alert alert-success">
                        {{ $status }}
                    </div>
                @endif
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if (session('warning'))
                    <div class="alert alert-warning">
                        {{ session('warning') }}
                    </div>
                @endif
                @if (!empty($warning))
                    <div class="alert alert-success">
                        {{ $warning }}
                    </div>
                @endif
                <div class="card-body card-padding" >
                    <div class="form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 radio" for="gender">Register As
                        </label>
                        <label class="radio radio-inline col-md-7 col-sm-8 col-xs-12" style="padding-bottom: 0px; margin-top: 0px;">
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="radio" name="role" value="tasker" required>
                            Tasker&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="radio" name="role" value="user" required>

                            Client

                        </label>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-md-3 control-label">Full Names</label>
                        <div class="col-md-7">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm {{ $errors->has('name') ? ' has-error' : '' }}" id="inputEmail3"
                                       placeholder="Full Names" name="name" value="{{ old('name') }}" required>
                                 @if ($errors->has('name'))
                                        <span class="help-block" style="color: #dc3545">
                                             <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="username" class="col-md-3 control-label">Username</label>
                        <div class="col-md-7">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm {{$errors->has('username') ? ' is-invalid' : '' }}" id="username"
                                       placeholder="Username" name="username" required value="{{ old('username') }}" >
                                @if ($errors->has('username'))
                                    <span class="invalid-feedback" style="color: #dc3545">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label">Email</label>
                        <div class="col-md-7">
                            <div class="fg-line">
                                <input type="email" class="form-control input-sm {{$errors->has('email') ? ' is-invalid' : '' }}" id="email"
                                       placeholder="Email" name="email" required value="{{ old('email') }}" >
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" style="color: #dc3545">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="phone" class="col-md-3 control-label">Phone</label>
                        <div class="col-md-7">
                            <div class="fg-line">
                                <input type="text" class="form-control input-sm input-mask {{$errors->has('phone') ? ' is-invalid' : '' }}" id="phone"
                                       placeholder="Phone" data-mask="+254700000000" value="+2547" name="phone" required >
                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" style="color: #dc3545">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">Password</label>
                        <div class="col-md-7">
                            <div class="fg-line">
                                <input type="password" class="form-control input-sm {{$errors->has('password') ? ' is-invalid' : '' }}" id="password"
                                       placeholder="Password" name="password" required  >
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" style="color: #dc3545">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="confirm_password" class="col-md-3 control-label">Confirm Password</label>
                        <div class="col-md-7">
                            <div class="fg-line">
                                <input type="password" class="form-control input-sm {{$errors->has('confirm_password') ? ' is-invalid' : '' }}" id="confirm_password"
                                       placeholder="Confirm Password" name="password_confirmation"  >
                                @if ($errors->has('confirm_password'))
                                    <span class="invalid-feedback" style="color: #dc3545">
                                        <strong>{{ $errors->first('confirm_password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="form-group" >
                        <label class="control-label col-md-3 col-sm-3 col-xs-12 radio" for="gender">Gender
                        </label>
                        <label class="radio radio-inline col-md-7 col-sm-8 col-xs-12" style="padding-bottom: 0px; margin-top: 0px;">
                            &nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="gender" value="Male" required>
                            Male&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="radio" name="gender" value="Female" required>
                            Female
                        </label>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-10">
                            <button type="submit" class="btn btn-primary btn-sm">Register</button>
                           <small>Already a member?</small> <a href="{{URL::to('login')}}" class="btn btn-link">Login</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@include('frontend.includes.footer')
<!-- //header -->

<script src="{{URL::to('')}}"></script>
<!-- start-smooth-scrolling -->
<script type="text/javascript" src="{{URL::to('js/move-top.js')}}"></script>
<script type="text/javascript" src="{{URL::to('js/easing.js')}}"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            // event.preventDefault();

            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- //end-smooth-scrolling -->
<!-- smooth-scrolling-of-move-up -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
         var defaults = {
         containerID: 'toTop', // fading element id
         containerHoverID: 'toTopHover', // fading element hover id
         scrollSpeed: 1200,
         easingType: 'linear'
         };
         */

        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{URL::to('js/bootstrap.js')}}"></script>
<script src="{{URL::to('js/custom.min.js')}}"></script>
<script src="{{URL::to('vendors/input-mask/input-mask.min.js')}}"></script>
</body>
</html>