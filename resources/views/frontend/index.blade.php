<!DOCTYPE html>
<html lang="en">
<head>
    <title>Kawindi Errands Information System</title>
    <!-- meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //meta tags -->
    <!-- custom Theme files -->
    <link href="{{URL::to('css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{URL::to('css/style2.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{URL::to('css/chocolat.css')}}" rel="stylesheet">
    <!-- //custom Theme files -->
    <!-- js -->
    <script src="{{URL::to('js/jquery-1.11.1.min.js')}}"></script>
    <!-- //js -->

</head>
<body>
<!-- header -->
@include('frontend.includes.header')
@include('frontend.includes.navigation')
@include('frontend.includes.banner')
@include('frontend.includes.about')
@include('frontend.includes.services')
@include('frontend.includes.footer')
<!-- //header -->

<script src="{{URL::to('js/smoothscroll.min.js')}}"></script>
<!-- start-smooth-scrolling -->
<script type="text/javascript" src="{{URL::to('js/move-top.js')}}"></script>
<script type="text/javascript" src="{{URL::to('js/easing.js')}}"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
           // event.preventDefault();

            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>`
<!-- //end-smooth-scrolling -->
<!-- smooth-scrolling-of-move-up -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
         var defaults = {
         containerID: 'toTop', // fading element id
         containerHoverID: 'toTopHover', // fading element hover id
         scrollSpeed: 1200,
         easingType: 'linear'
         };
         */

        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{URL::to('js/bootstrap.js')}}"></script>
</body>
</html>