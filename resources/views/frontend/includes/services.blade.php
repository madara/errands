

<!-- services -->
<div id="services" class="services">
    <div class="container">
        <h2 class="w3ls-tittle">Services</h2>
        <div class="services-grids w3layouts-grids">
            <div class="col-md-6 services-left">
                <div class="serw3agile-grid">
                    <span class="hi-icon hi-icon-archive glyphicon glyphicon-check"> </span>
                </div>
                <div  class="ser-agiletop">
                   <a href="#"> <h4>Mama Fua</h4></a>
                    <p>Quality work from trusted and verified cleaners. </p>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="col-md-6 services-left">
                <div class="serw3agile-grid">
                    <span class="hi-icon hi-icon-archive glyphicon glyphicon-wrench"> </span>
                </div>
                <div  class="ser-agiletop">
                    <h4>Electricians</h4>
                    <p>Get experinced electrians work on your electrical devices  </p>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="col-md-6 services-left">
                <div class="serw3agile-grid">
                    <span class="hi-icon hi-icon-archive glyphicon glyphicon-scale"> </span>
                </div>
                <div  class="ser-agiletop">
                    <h4>Carpenters</h4>
                    <p>For all wood work repairs and timely deliveries we have got you covered. </p>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="col-md-6 services-left">
                <div class="serw3agile-grid">
                    <span class="hi-icon hi-icon-archive glyphicon  glyphicon-cog"> </span>
                </div>
                <div  class="ser-agiletop">
                    <h4>Plumbers</h4>
                    <p>Messy and leaking taps are not comfortabe to be with,we  have got you sorted out. </p>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- //services -->