<!-- Forgot Password -->

<div class="lc-block" id="l-forget-password">
    <form method="POST" action="#">
        {{csrf_field()}}
        <div class="lcb-form">
            <p class="text-left">Reset Password</p>
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif

            <div class="input-group m-b-20 ">
                <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                <div class="fg-line {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" id="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Email Address">
                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                </span>
                    @endif
                </div>
            </div>

            <button class="btn btn-login btn-success btn-float"  type="submit"><i class="zmdi zmdi-arrow-forward"></i></button>
        </div>
    </form>

    <div class="lcb-navigation">
        <a href="" data-ma-action="login-switch" data-ma-block="#l-login"><i class="zmdi zmdi-long-arrow-right"></i> <span>Sign in</span></a>
        <a href="" data-ma-action="login-switch" data-ma-block="#l-register"><i class="zmdi zmdi-plus"></i> <span>Register</span></a>

    </div>

</div>