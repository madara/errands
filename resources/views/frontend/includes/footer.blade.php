<!-- footer -->
<div class="footer">
    <div class="container">
        <div class="footer-left agileits-w3layouts">
            <p>&copy; Kawindi Errands. All Rights Reserved | Design by  <a href="https://www.linkedin.com/in/ian-madara-a310a4a7/" target="_blank">Kawindi</a></p>
        </div>
        <div class="footer-right">
            <ul>
                <li><a href="#" class="f1"></a></li>
                <li><a href="#" class="f2"></a></li>
                <li><a href="#" class="f3"></a></li>
                <li><a href="#" class="f4"></a></li>
            </ul>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>
<!-- //footer -->