<!-- banner -->
<div class="banner">
    <div class="container">
        <script src="{{URL::to('js/responsiveslides.min.js')}}"></script>
        <script>
            // You can also use "$(window).load(function() {"
            $(function () {
                // Slideshow 4
                $("#slider3").responsiveSlides({
                    auto: true,
                    pager: true,
                    nav: false,
                    speed: 500,
                    namespace: "callbacks",
                    before: function () {
                        $('.events').append("<li>before event fired.</li>");
                    },
                    after: function () {
                        $('.events').append("<li>after event fired.</li>");
                    }
                });
            });
        </script>
        <div  id="top" class="callbacks_container">
            <ul class="rslides" id="slider3">
                <li>
                    <div class="banner-agileinfo">
                        <h3>WELCOME</h3>
                        <h4>Handiman & casual jobs services</h4>
                    </div>
                </li>
                <li>
                    <div class="banner-agileinfo">
                        <h3>WELCOME</h3>
                        <h4>Get all your tasks done by our team</h4>
                    </div>
                </li>
                <li>
                    <div class="banner-agileinfo">
                        <h3>WELCOME</h3>
                        <h4>We help you plan your errands</h4>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- //banner -->