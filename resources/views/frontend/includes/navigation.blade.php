<!-- navigation -->
<div class="navigation">
    <div class="container">
        <div class="agileits-logo">
            <h1><a href="{{route('/')}}"><span> Errands</span>KAWINDI</a></h1>
        </div>
        <nav class="navbar navbar-default">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li class="hvr-bounce-to-bottom button"><a href="{{route('/')}}" class="scroll">Home<span class="sr-only">(current)</span></a></li>
                    <li class="hvr-bounce-to-bottom button"><a href="{{route('front.taskers')}}" class="scroll">Taskers</a></li>
                    <li class="hvr-bounce-to-bottom button"><a href="#services" class="scroll">Services</a></li>
                </ul>
                <div class="social-icons">
                    <ul>
                        <li><a href="#" class="f1"></a></li>
                        <li><a href="#" class="f2"></a></li>
                        <li><a href="#" class="f3"></a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div><!-- /.navbar-collapse -->
        </nav>
    </div>
</div>
<!-- //navigation -->