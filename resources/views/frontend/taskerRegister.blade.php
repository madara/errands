<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Register As Tasker</title>

    <!-- Vendor CSS -->
    <link href="{{URL::to('vendors/bower_components/animate.css/animate.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css')}}" rel="stylesheet">

    <!-- CSS -->
    <link href="{{URL::to('css/app_1.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('css/app_2.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/farbtastic/farbtastic.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/chosen/chosen.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/sweetalert/dist/sweetalert.css')}}" rel="stylesheet">
    <script src="{{URL::to('vendors/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>



<body >

<div class="login-content">
    <!--Register -->
    <div class="lc-block toggled" id="l-register">
        <form method="POST" action="{{route('user.register')}}">
            {{csrf_field()}}
            <input type="hidden" name="role" value="tasker">

            <div class="lcb-form">
                <p class="text-left">Register</p>
                @if (!empty($status))
                    <div class="alert alert-success">
                        {{ $status }}
                    </div>
                @endif
                @if (!empty($warning))
                    <div class="alert alert-success">
                        {{ $warning }}
                    </div>
                @endif

                <div class="input-group fg-float m-b-20 ">
                    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                    <div class="fg-line {{ $errors->has('name') ? ' has-error' : '' }}">
                        <input type="text" id="name" required name="name" class="form-control" value="{{ old('name') }}">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                        <label class="fg-label">Full Names</label>
                    </div>
                </div>
                <div class="input-group fg-float m-b-20 ">
                    <span class="input-group-addon"><i class="zmdi zmdi-pin-account"></i></span>
                    <div class="fg-line {{ $errors->has('username') ? ' has-error' : '' }}">
                        <input type="text" id="username" name="username" class="form-control" value="{{ old('username') }}"  required>
                        @if ($errors->has('username'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                            </span>
                        @endif
                        <label class="fg-label">Username</label>
                    </div>
                </div>
                <div class="input-group fg-float m-b-20 ">
                    <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                    <div class="fg-line {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" id="email" name="email" class="form-control" value="{{ old('email') }}"  required>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <label class="fg-label">Email</label>
                    </div>
                </div>
                <div class="input-group fg-float m-b-20">
                    <span class="input-group-addon"><i class="zmdi zmdi-phone"></i></span>
                    <div class="fg-line{{ $errors->has('phone') ? ' has-error' : '' }}">
                        <input type="text" class="form-control input-mask" data-mask="+254700000000"  name="phone" value="+2547" required>
                        @if ($errors->has('phone'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                            </span>
                        @endif
                        <label class="fg-label">Phone</label>
                    </div>
                </div>
                <div class="input-group fg-float m-b-20">
                    <span class="input-group-addon"><i class="zmdi zmdi-layers"></i></span>
                    <div class="fg-line{{ $errors->has('id_no') ? ' has-error' : '' }}">
                        <input type="text" class="form-control input-mask" data-mask="000000000"  name="id_no"  required>
                        @if ($errors->has('phone'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('id_no') }}</strong>
                            </span>
                        @endif
                        <label class="fg-label">ID Number</label>
                    </div>
                </div>

                <div class="input-group fg-float m-b-20">
                    <span class="input-group-addon"><i class="zmdi zmdi-male"></i></span>
                    <div class="fg-line{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" id="password" class="form-control"  name="password" required="required">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                        <label class="fg-label">Password</label>
                    </div>
                </div>

                <div class="input-group fg-float m-b-20">
                    <span class="input-group-addon"><i class="zmdi zmdi-male"></i></span>
                    <div class="fg-line{{ $errors->has('confirm_password') ? ' has-error' : '' }}">
                        <input type="password" id="confirm_password" class="form-control"  name="password_confirmation" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('confirm_password') }}</strong>
                            </span>
                        @endif
                        <label class="fg-label">Confirm Password</label>
                    </div>
                </div>
                <div class="input-group m-b-20">
                    <span class="input-group-addon"><i class="zmdi zmdi-male-female"></i></span>
                    <label class="radio radio-inline m-r-20">
                        <input type="radio" name="gender" value="Male" >
                        <i class="input-helper"></i>
                        Male
                    </label>

                    <label class="radio radio-inline m-r-20">
                        <input type="radio" name="gender" value="Female" >
                        <i class="input-helper"></i>
                        Female
                    </label>


                </div>

                <button class="btn btn-login btn-success btn-float"  type="submit"><i class="zmdi zmdi-arrow-forward"></i></button>
            </div>
        </form>

        <div class="lcb-navigation">
            <a href="" data-ma-action="login-switch" data-ma-block="#l-login"><i class="zmdi zmdi-long-arrow-right"></i> <span>Sign in</span></a>
            <a href="" data-ma-action="login-switch" data-ma-block="#l-forget-password"><i>?</i> <span>Forgot Password</span></a>

        </div>

    </div>
    <div class="lc-block" id="l-login">
        <form action="{{route('login')}}" method="POST">
        {!!csrf_field()!!}
        <!-- Login -->
            <div class="lcb-form">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="input-group m-b-20">
                    <span class="input-group-addon"><i class="zmdi zmdi-account"></i></span>
                    <div class="fg-line{{$errors->has('username') ? ' has-error' : '' }}">
                        <input type="text" class="form-control" placeholder="Username" name="username" required="required">
                        @if ($errors->has('username'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="input-group m-b-20">
                    <span class="input-group-addon"><i class="zmdi zmdi-male"></i></span>
                    <div class="fg-line{{$errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" class="form-control" placeholder="Password" name="password" id="password" required="required">
                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="">
                        <i class="input-helper"></i>
                        Keep me signed in
                    </label>
                </div>

                <button class="btn btn-login btn-success btn-float" id="login" type="submit"><i class="zmdi zmdi-arrow-forward"></i></button>
            </div>
        </form>

        <div class="lcb-navigation">
            <a href="" data-ma-action="login-switch" data-ma-block="#l-register"><i class="zmdi zmdi-plus"></i> <span>Register</span></a>
            <a href="" data-ma-action="login-switch" data-ma-block="#l-forget-password"><i>?</i> <span>Forgot Password</span></a>
        </div>
    </div>

    <!-- Forgot Password -->

    <div class="lc-block" id="l-forget-password">
        <form method="POST" action="">
            {{csrf_field()}}
            <div class="lcb-form">
                <p class="text-left">Reset Password</p>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="input-group m-b-20 ">
                    <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                    <div class="fg-line {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" id="email" name="email" class="form-control" value="{{ old('email') }}" placeholder="Email Address">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>

                <button class="btn btn-login btn-success btn-float"  type="submit"><i class="zmdi zmdi-arrow-forward"></i></button>
            </div>
        </form>

        <div class="lcb-navigation">
            <a href="" data-ma-action="login-switch" data-ma-block="#l-login"><i class="zmdi zmdi-long-arrow-right"></i> <span>Sign in</span></a>
            <a href="" data-ma-action="login-switch" data-ma-block="#l-register"><i class="zmdi zmdi-plus"></i> <span>Register</span></a>

        </div>

    </div>



</div>
<!-- Javascript Libraries -->
<script src="{{URL::to('vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{URL::to('vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<script src="{{URL::to('vendors/bower_components/Waves/dist/waves.min.js')}}"></script>
<script src="{{URL::to('js/app.min.js')}}"></script>
<script src="{{URL::to('vendors/bower_components/chosen/chosen.jquery.js')}}"></script>
<script src="{{URL::to('vendors/fileinput/fileinput.min.js')}}"></script>
<script src="{{URL::to('vendors/input-mask/input-mask.min.js')}}"></script>
<script src="{{URL::to('vendors/farbtastic/farbtastic.min.js')}}"></script>
</body>
</html>