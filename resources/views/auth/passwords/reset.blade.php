<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Reset Password</title>

    <!-- Vendor CSS -->
    <link href="{{URL::to('vendors/bower_components/animate.css/animate.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css')}}" rel="stylesheet">

    <!-- CSS -->
    <link href="{{URL::to('css/app_1.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('css/app_2.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/farbtastic/farbtastic.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/chosen/chosen.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/sweetalert/dist/sweetalert.css')}}" rel="stylesheet">
    <script src="{{URL::to('vendors/bower_components/sweetalert/dist/sweetalert.min.js')}}"></script>



<body >

<div class="login-content">
    <!-- Reset Password -->

    <div class="lc-block toggled" id="l-forget-password">
        <form method="POST" action="{{ route('password.request') }}">
            {{csrf_field()}}
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="lcb-form">
                <p class="text-left">Reset Password</p>
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="input-group m-b-20 ">
                    <span class="input-group-addon"><i class="zmdi zmdi-email"></i></span>
                    <div class="fg-line {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" id="email" name="email" class="form-control" value="{{ $email or old('email') }}" required autofocus placeholder="Email Address">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>
                <div class="input-group m-b-20 ">
                    <span class="input-group-addon"><i class="zmdi zmdi-male"></i></span>
                    <div class="fg-line {{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" id="password" name="password" class="form-control" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>
                <div class="input-group m-b-20 ">
                    <span class="input-group-addon"><i class="zmdi zmdi-male"></i></span>
                    <div class="fg-line {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                        @endif
                    </div>
                </div>

                <button class="btn btn-login btn-success btn-float"  type="submit"><i class="zmdi zmdi-arrow-forward"></i></button>
            </div>
        </form>


    </div>


</div>
<!-- Javascript Libraries -->
<script src="{{URL::to('vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script src="{{URL::to('vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<script src="{{URL::to('vendors/bower_components/Waves/dist/waves.min.js')}}"></script>
<script src="{{URL::to('js/app.min.js')}}"></script>
<script src="{{URL::to('vendors/bower_components/chosen/chosen.jquery.js')}}"></script>
<script src="{{URL::to('vendors/fileinput/fileinput.min.js')}}"></script>
<script src="{{URL::to('vendors/input-mask/input-mask.min.js')}}"></script>
<script src="{{URL::to('vendors/farbtastic/farbtastic.min.js')}}"></script>
</body>
</html>