@extends('tasker.master')
@section('profile')
    <style type="text/css">
        #profile-main {
            min-height: 1020px;
            max-height: inherit;
        }

        #clearfix {
            min-width: 732px;
        }

        #center {
            min-height: 400px;
            max-height: 400px;
            max-width: 400px;
            min-width: 400px;
        }

        #pic {
            min-height: 300px;
            max-height: 300px;
        }

        #f-menu {
            display: inline-block;

            padding-left: 0;
            margin-left: -5px;
            margin-top: 5px;
            list-style: none;
        }

        #f-menu li {
            display: inline-block;
            padding-left: 5px;
            padding-right: 5px;

        }
    </style>
    <div class="container container-alt ">

        <div class="block-header">
            <h2>{{$user->name}}
                <small>@if($user->skill_id==!null){{$user->skill->skill}}@endif at {{$location}}</small>
            </h2>
        </div>
        @if(Session::has('profileMessage'))
            <p class="alert alert-warning">{{ Session::get('profileMessage') }}</p>
        @endif

        <div class="card col-md-3 profile" id="profile-main">
            <div class="pm-overview c-overflow">
                <div class="pmo-pic">
                    <div class="p-relative">
                        <a href="#">

                            @if(Auth::user()->image===null)
                                @if(Auth::user()->gender=='Male')
                                    <img src="{{url('img/profile-pics/male.jpg')}}" alt="">
                                @elseif(Auth::user()->gender=='Female')
                                    <img src="{{url('img/profile-pics/male.jpg')}}" alt="">
                                @endif
                            @else
                                <img id="pic" class="img-responsive" src="{{URL::to('/profile/'. $user->image)}}"
                                     alt="">
                            @endif
                        </a>

                        <a href="#" id="photo" class="pmop-edit">
                            <i class="zmdi zmdi-camera"></i> <span class="hidden-xs">Update Profile Picture</span>
                        </a>
                    </div>

                </div>
            </div>

        </div>
        <div class="pm-body clearfix col-md-9" id="clearfix">
            <div class="container container-alt">
                <div class="card" id="profile-main">

                    <div class="pm-body clearfix">
                        <div class="pmb-block">
                            <div class="pmbb-header">
                                <h2><i class="zmdi zmdi-equalizer m-r-10"></i> Summary</h2>

                                <ul class="actions">
                                    <li class="dropdown">
                                        <a href="" data-ma-action="profile-edit">
                                            <button class="btn btn-success btn-icon"><i class="zmdi zmdi-edit"></i>
                                            </button>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="pmbb-body p-l-30">
                                <div class="pmbb-view">
                                    {{$user->summary}}
                                </div>
                                <form method="POST" action="{{route('tasker.summary')}}">
                                    {{csrf_field()}}
                                    <div class="pmbb-edit">
                                        <div class="fg-line">
                                            <textarea class="form-control" rows="5" name="summary"
                                                      placeholder="Summary...">{{$user->summary}}</textarea>
                                        </div>
                                        <div class="m-t-10">
                                            <button class="btn btn-primary btn-sm">Save</button>
                                            <button data-ma-action="profile-edit-cancel" class="btn btn-link btn-sm">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="pmb-block ">
                            <div class="pmbb-header">
                                <h2><i class="zmdi zmdi-account m-r-10"></i> Basic Information</h2>

                                <ul class="actions">
                                    <li class="dropdown">
                                        <a href="" data-ma-action="profile-edit">
                                            <button class="btn btn-success btn-icon"><i class="zmdi zmdi-edit"></i>
                                            </button>
                                        </a>

                                    </li>
                                </ul>
                            </div>
                            <div class="pmbb-body p-l-30">

                                <div class="pmbb-view">
                                    <dl class="dl-horizontal">
                                        <dt>Full Names</dt>
                                        <dd>{{$user->name}}</dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt>ID</dt>
                                        <dd>{{$user->id_no}}</dd>
                                    </dl>

                                    <dl class="dl-horizontal">
                                        <dt>Gender</dt>
                                        <dd>{{$user->gender}}</dd>
                                    </dl>
                                </div>
                                <form method="POST" action="{{route('tasker.updateBasic')}}">
                                    {{csrf_field()}}
                                    <div class="pmbb-edit">
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Full Names</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type="text" name="name" class="form-control"
                                                           value="{{$user->name}}">
                                                </div>

                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">ID</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type="text" name="id_no" class="form-control input-mask"
                                                           value="{{$user->id_no}}" data-mask="000000000">
                                                </div>

                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Gender</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <select class="form-control selectpicker" name="gender">
                                                        @if($user->gender=='Male')
                                                            <option selected="selected" value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                        @elseif($user->gender=='Female')
                                                            <option selected="selected" value="Female">Female</option>
                                                            <option value="Male"> Male</option>
                                                        @else
                                                            <option value="Male">Male</option>
                                                            <option value="Female">Female</option>
                                                        @endif

                                                    </select>
                                                </div>
                                            </dd>
                                        </dl>


                                        <div class="m-t-30">
                                            <button class="btn btn-primary btn-sm" type="submit">Save</button>
                                            <button data-ma-action="profile-edit-cancel" class="btn btn-link btn-sm">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="pmb-block">
                            <div class="pmbb-header">
                                <h2><i class="zmdi zmdi-phone m-r-10"></i> Contact Information</h2>

                                <ul class="actions">
                                    <li class="dropdown">
                                        <a href="" data-ma-action="profile-edit">
                                            <button class="btn btn-xs btn-success  btn-icon"><i
                                                        class="zmdi zmdi-edit"></i></button>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="pmbb-body p-l-30">
                                <div class="pmbb-view">
                                    <dl class="dl-horizontal">
                                        <dt>Mobile Phone</dt>
                                        <dd>{{$user->phone}}</dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt>Email Address</dt>
                                        <dd>{{$user->email}}</dd>
                                    </dl>

                                </div>
                                <form method="POST" action="{{route('tasker.updateContact')}}">
                                    {{csrf_field()}}
                                    <div class="pmbb-edit">
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Mobile Phone</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type="text" class="form-control input-mask" name="phone"
                                                           value="{{$user->phone}}" data-mask="+254700000000">
                                                </div>
                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Email Address</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type="email" class="form-control" name="email"
                                                           value="{{$user->email}}">
                                                </div>
                                            </dd>
                                        </dl>

                                        <div class="m-t-30">
                                            <button class="btn btn-primary btn-sm" type="submit">Save</button>
                                            <button data-ma-action="profile-edit-cancel" class="btn btn-link btn-sm">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="pmb-block">
                            <div class="pmbb-header">
                                <h2><i class="zmdi zmdi-my-location m-r-10"></i> Location Information</h2>

                                <ul class="actions">
                                    <li class="dropdown">
                                        <a href="" data-ma-action="profile-edit">
                                            <button class="btn btn-success btn-icon"><i class="zmdi zmdi-edit"></i>
                                            </button>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="pmbb-body p-l-30">
                                <div class="pmbb-view">
                                    <dl class="dl-horizontal">
                                        <dt>Location</dt>
                                        <dd>{{$location}}</dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt>Landmark</dt>
                                        <dd>{{$user->landmark}}</dd>
                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt>Direction</dt>
                                        <dd>{{$user->direction}}</dd>
                                    </dl>

                                </div>

                                <form method="POST" action="{{route('tasker.updatelocation')}}">
                                    {{csrf_field()}}
                                    <div class="pmbb-edit">
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Location</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <select class=" formcontrol selectpicker" name="location" required>
                                                        <option value="" selected>--select--</option>
                                                        @foreach($locations as $place)
                                                            <option value="{{$place->id}}">{{$place->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Landmark</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <input type="text" class="form-control" name="landmark"
                                                           value="{{$user->landmark}}">
                                                </div>
                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Direction</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <textarea class="form-control"
                                                              name="direction">{{$user->direction}}</textarea>
                                                </div>
                                            </dd>
                                        </dl>


                                        <div class="m-t-30">
                                            <button class="btn btn-primary btn-sm" type="submit">Save</button>
                                            <button data-ma-action="profile-edit-cancel" class="btn btn-link btn-sm">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="pmb-block">
                            <div class="pmbb-header">
                                <h2><i class="zmdi zmdi-assignment-account m-r-10"></i> Skills</h2>

                                <ul class="actions">
                                    <li class="dropdown">
                                        <a href="" data-ma-action="profile-edit">
                                            <button class="btn btn-success btn-icon"><i class="zmdi zmdi-edit"></i>
                                            </button>
                                        </a>
                                    </li>
                                </ul>
                                @if (session('message'))
                                    <div class="alert alert-warning">
                                        {{ session('message') }}
                                    </div>
                                @endif
                            </div>
                            <div class="pmbb-body p-l-30">
                                <div class="pmbb-view">
                                    <dl class="dl-horizontal">
                                        <dt>Main</dt>
                                        @if($user->skill_id==!null)
                                            <dd>{{$user->skill->skill}}</dd>
                                        @endif

                                    </dl>
                                    <dl class="dl-horizontal">
                                        <dt>Others</dt>
                                        <dd>
                                            @if($retrieved==!"")
                                                @foreach($retrieved as $retrieve)
                                                    <ul id="f-menu">
                                                        <li>{{App\Skill::where('id',$retrieve)->first()->skill}}</li>
                                                    </ul>
                                                @endforeach
                                            @endif
                                        </dd>
                                    </dl>

                                </div>
                                <form method="POST" action="{{route('tasker.updateskills')}}">
                                    {{csrf_field()}}
                                    <div class="pmbb-edit">
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Skill</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <select class="form-control selectpicker" name="skill_id" required>
                                                        <option value="" selected>--Select--</option>
                                                        @foreach($skills as $skill)
                                                            <option value="{{$skill->id}}">{{$skill->skill}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </dd>
                                        </dl>
                                        <dl class="dl-horizontal">
                                            <dt class="p-t-10">Other</dt>
                                            <dd>
                                                <div class="fg-line">
                                                    <select class="form-control selectpicker" multiple
                                                            data-max-options="2" name="other_skill[]">

                                                        @foreach($skills as $skill)
                                                            <option value="{{$skill->id}}">{{$skill->skill}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </dd>
                                        </dl>

                                        <div class="m-t-30">
                                            <button class="btn btn-primary btn-sm" type="submit">Save</button>
                                            <button data-ma-action="profile-edit-cancel" class="btn btn-link btn-sm">
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="updatePhoto" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span
                                class="sr-only">Close</span></button>
                    <h3 class="modal-title" id="lineModalLabel">Edit Photo</h3>
                </div>
                <div class="modal-body">

                    <!-- content goes here -->
                    <form id="updateform" method="POST" action="{{route('tasker.updatephoto')}}"
                          enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="center" style="text-align: center">
                            <div class="fileinput fileinput-new " data-provides="fileinput">
                                <div id="center" class="fileinput-preview thumbnail " data-trigger="fileinput"></div>
                                <div>
                                    <span class="btn btn-info btn-file">
                                        <span class="fileinput-new">Select image</span>
                                        {{--<span class="fileinput-exists"> Change</span>--}}
                                        <input type="file" name="image">
                                    </span>
                                    <a href="#" class="btn btn-danger fileinput-exists"
                                       data-dismiss="fileinput">Remove</a>
                                </div>

                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <div class="btn-group btn-group-justified" role="group" aria-label="group button">

                        <div class="btn-group" role="group">
                            <button type="submit" class="btn btn-primary btn-hover-green" data-action="save"
                                    role="button">Update
                            </button>
                        </div>
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-default" data-dismiss="modal" role="button">Close
                            </button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $('#photo').on('click', function (e) {
            e.preventDefault()
            $('#updatePhoto').modal('show');


        })
    </script>
@endsection
@section('scripts')

@endsection