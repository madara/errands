<aside id="sidebar" class="sidebar c-overflow">
                <div class="s-profile">
                    <a href="" data-ma-action="profile-menu-toggle">
                        <div class="sp-pic">
                            @if(Auth::user()->image==!null)
                                <img src="{{url('profile/'.Auth::user()->image)}}" alt="">
                            @else
                                @if(Auth::user()->gender=='Male')
                                    <img src="{{url('img/profile-pics/male.jpg')}}" alt="">
                                @elseif(Auth::user()->gender=='Female')
                                    <img src="{{url('img/profile-pics/male.jpg')}}" alt="">
                                @endif

                            @endif
                        </div>

                        <div class="sp-info">
                            {{Auth::user()->name}}
                            <i class="zmdi zmdi-caret-down"></i>
                        </div>
                    </a>

                    <ul class="main-menu">

                        <li>
                            <a href="{{route('tasker.profile')}}"><i class="zmdi zmdi-account"></i> View Profile</a>
                        </li>
                        <li>
                            <a href="{{route('tasker.password')}}"><i class="zmdi zmdi-input-antenna"></i> Change Password</a>
                        </li>
                        <li>
                            <a href="{{route('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                <i class="zmdi zmdi-time-restore"></i> Logout</a>
                            <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                                {{csrf_field()}}
                            </form>
                        </li>
                    </ul>
                </div>

                <ul class="main-menu">
                    <li class="active">
                        <a href="{{route('tasker.index')}}"><i class="zmdi zmdi-home"></i> Home</a>
                    </li>
                    <li class="sub-menu">
                        <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-account"></i> Profile</a>

                        <ul>
                            <li><a href="{{route('tasker.profile')}}">View</a></li>
                            <li><a href="#" >Skills</a> </li>
                        </ul>
                    </li>  
                    
                    <li class="sub-menu">
                        <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-widgets"></i> Clients</a>
     
                        <ul>
                            @if($subscription_status==0 || $remainingDays==0)
                            <li><a href="{{route('tasker.paymentExpired')}}">View</a></li>
                             @elseif($subscription_status==1 && $approved==1)
                                <li><a href="{{route('tasker.viewclients')}}">View</a></li>
                            @elseif($subscription_status==1 && $approved==0)
                                <li><a href="{{route('tasker.waitApproval')}}">View</a></li>
                             @endif

                        </ul>
                        
                    </li>
                    <li class="sub-menu">
                        <a href="" data-ma-action="submenu-toggle"><i class="zmdi zmdi-money-box"></i>Subcriptions</a>
                        <ul>
                            <li><a href="{{route('tasker.subscription')}}">Make Subscription</a></li>
                            <li><a href="{{route('tasker.viewpayment')}}">Payment Details</a></li>
                        </ul>
                    </li>


                </ul>
            </aside>