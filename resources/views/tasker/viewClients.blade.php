@extends('tasker.master')
@if(\App\Payment::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->first()->status==0 && $remainingDays==0)
    <?php
    header("refresh:4;url=http://localhost/errands/public/tasker/payment/wait-admin-approval")
    ?>
@endif
@section('content')
    <div class="row">
        <div class="card">
            <div class="card-header">
                <h2>My Clients</h2>
                <small>All your hiring requests and their progress</small>
            </div>
            <div class="card-body card-padding">
                <table class="table table-striped table-bordered table-vmiddle responsive">
                    <thead>
                    <tr>
                        <th>Client Name</th>
                        <th>Date Booked</th>
                        <th>Time</th>
                        <th>Hire Status</th>
                        <th>Completed</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($clients as $client)
                        <tr>
                            <td onclick="return submitForm('{{$client->id}}')">{{$client->name}}</td>

                            <td class="center date" id="date">{{Carbon\Carbon::parse($client->pivot->date)->format('d-m-Y')}}</td>

                            <td class="center time" id="time"> {{$client->pivot->time}}</td>
                            @if($client->pivot->status===null)
                            <td class="center"><button class="btn btn-warning btn-xs">Pending</button> </td>
                            @elseif($client->pivot->status==1)
                            <td class="center"><button class="btn btn-success btn-xs"><i class="zmdi zmdi-check-all"></i>Accepted</button></td>
                            @elseif($client->pivot->status==0)
                                <td class="center"><button class="btn btn-danger btn-xs"><i class="zmdi zmdi-close-circle-o"></i> Client Canceled</button></td>
                            @elseif($client->pivot->status==2)
                            <td class="center"><button class="btn btn-danger btn-xs"><i class="zmdi zmdi-block"></i> Rejected</button></td>
                            @endif
                            @if($client->pivot->completed==null)
                                <td class="center"><button class="btn btn-warning btn-xs" onclick="return submitClient('{{$client->pivot->id}}')" >Pending</button> </td>
                            @elseif($client->pivot->completed==1)
                                <td class="center"><button class="btn btn-xs bgm-lightgreen"><i class="zmdi zmdi-trending-up"></i> Waiting Approval </button></td>
                            @elseif($client->pivot->completed==2)
                                <td class="center"><button class="btn btn-success btn-xs"><i class="zmdi zmdi-check-all"></i>Completed</button></td>
                            @endif
                            <td class="center">
                                <a href="#">
                                    <button type="button" onclick="return submitClient('{{$client->pivot->id}}')"  class="btn btn-xs btn-success" >
                                        <i class="zmdi zmdi-eye"></i> View
                                    </button>
                                </a>
                            </td>
                        </tr>

                        <form method="GET" style="visibility: hidden;" action="{{route('tasker.viewclient', $client->id)}}" id="{{$client->pivot->id}}">
                            {{csrf_field()}}
                            <input type="hidden" name="time"  value="{{$client->pivot->time}}">
                            <input type="hidden" name="date"  value="{{$client->pivot->date}}">
                            <input type="hidden" name="schedule_id" id="schedule_id" value="{{$client->pivot->id}}">

                        </form>
                    @empty
                        <tr>
                            <td colspan="6" style="text-align: center; color: #03A9F4;">You don't have any errands at the moment</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
 <script type="text/javascript">
     function  submitClient(id) {
         //alert(id);
        document.getElementById(id).submit()
     }
 </script>
@endsection