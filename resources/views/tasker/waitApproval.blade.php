@extends('tasker.master')
<?php
header("refresh:4;url=http://localhost/errands/public/tasker/view/clients")
?>
@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Clients
                <small>All your hiring requests and their progress.</small>
            </h2>

        </div>
        <div class="card-body card-padding">
            <div class="row">
                @if($message)
                    <p class="alert alert-warning">{{ $message }}</p>
                @endif
            </div>

        </div>
    </div>
@endsection
