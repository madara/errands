@extends('tasker.master')
@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Payment Procedure
            <small>Please Note that payment is Ksh 100 per month starting from the current date.</small>
            </h2>

        </div>
        <div class="card-body card-padding">
            <div class="row">
                <div class="col-lg-7 m-b-25 m-r-25 f-16">
                    <ol>
                        <li> Go to M-PESA on your phone</li>
                        <li>Select Pay Bill option</li>
                        <li>Enter Business no. 608608</li>
                        <li>Enter Account no. KAWINDI</li>
                        <li>Enter the Amount. KES 200.00</li>
                        <li>Enter your M-PESA PIN and Send</li>
                        <li>You will receive a confirmation SMS from MPESA</li>
                    </ol>
                </div>

            </div>
            <button class="btn bgm-bluegray" id="save"><i class=""></i>Confirm Payment</button>
        </div>
    </div>
    <div class="modal fade" id="payment" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Subcription Payment</h4>
                </div>
                <div class="modal-body">
                    <!-- content goes here -->
                    <form action="{{route('tasker.payment')}}" method="POST">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="title">Amount Paid</label>
                            <input type="text" class="form-control input-mask" data-mask="0000.00" name="amount" id="amount" placeholder="Amount Paid" required>
                        </div>
                        <div class="form-group">
                            <label for="title">MPESA Receipt</label>
                            <input type="text" class="form-control" name="receipt" id="receipt" required placeholder="MPESA Transaction Receipt">
                        </div>


                        <div class="modal-footer">

                            <button type="submit"   class="btn btn-primary btn-hover-green btn-sm pull-left" data-action="save" role="button" >Add</button>

                            <button type="button" class="btn btn-default" data-dismiss="modal"  role="button">Close</button>

                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>


@endsection
@section('scripts')
<script type="text/javascript">
    $('#save').on('click', function (e) {
        e.preventDefault();
        $('#payment').modal('show');
    })
</script>
@endsection