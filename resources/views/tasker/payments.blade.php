@extends('tasker.master')

@section('content')
    <div class="card">
        <div class="card-header">
            <h2>Payments
            </h2>
        </div>

        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Date</th>
                    <th>Receipt</th>
                    <th>Amount</th>
                    <th>Days</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                @forelse($payments as $key=>$payment)
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$payment->created_at->toDateString()}}</td>
                        <td>{{$payment->receipt}}</td>
                        <td>{{$payment->amount}}</td>
                        <td>{{$payment->days}}</td>
                        @if($payment->status==1)
                            <td><button class="btn btn-success btn-xs"><i class="zmdi zmdi-check-all"></i>Approved </button></td>
                        @else
                            <td><button class="btn btn-warning btn-xs">Pending</button></td>
                        @endif
                    </tr>
                @empty
                    <tr>
                        <td colspan="5" style="text-align: center; color: #03A9F4;">You Have not made any payment at the moment</td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>

        <div class="col-xs-12">
            <br>
            <div class=" brd-2 p-15">
                <div class=" m-b-5">Your Subscription Expires after</div>
                <h3 class="m-0 c-red f-100"> {{$remainingDays}} days</h3>
            </div>
        </div>
    </div>
@endsection