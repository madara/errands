@extends('tasker.master')
@section('content')
    <style type="text/css">
        .pmbb-header{
            margin-bottom:25px;
            position:relative;

        }
        .pmbb-header h2{
            margin:0;
            font-weight:100;
            font-size:20px;
        }
        #f-menu{
            display: inline-block;

            padding-left: 0;
            margin-left: -5px;
            margin-top: 5px;
            list-style: none;
        }
        #f-menu li{
            display: inline-block;
            padding-left: 5px;
            padding-right: 5px;

        }
    </style>

    <div class="timeline">
        <div class="t-view" data-tv-type="text">
            <div class="tv-header media">
                <a href="" class="tvh-user pull-left">
                    <img class="img-responsive" src="{{URL::to('profile/'. $user->image)}}" alt="">
                </a>
                <div class="media-body p-t-5">
                    <strong class="d-block">{{$user->name}}</strong>
                    <small class="c-gray">{{'Currently at '. $user->location->name}}</small>
                </div>
            </div>
            <div class="tv-body">
                <div class="clearfix"></div>

                <div class="clearfix"></div>
                <div class="pm-body clearfix">
                    <div class="pmb-block ">
                        <div class="pmbb-header">
                            <h2><i class="zmdi zmdi-pin-account m-r-10"></i>Contact Information</h2>
                        </div>
                        <div class="pmbb-body p-l-30">

                            <div class="pmbb-view">
                                <dl class="dl-horizontal">
                                    <dt>Mobile Phone</dt>
                                    <dd>{{$user->phone}}</dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Email Address</dt>
                                    <dd>{{$user->email}}</dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Location</dt>
                                    <dd>{{$user->location->name}}</dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Direction</dt>
                                    <dd>{{$user->direction}}</dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Landmark</dt>
                                    <dd>{{$user->landmark}}</dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pm-body clearfix">
                    <div class="pmb-block ">
                        <div class="pmbb-header">
                            <h2><i class="zmdi zmdi-calendar m-r-10"></i>Schedule</h2>
                        </div>
                        <div class="pmbb-body p-l-30">

                            <div class="pmbb-view">
                                <dl class="dl-horizontal">
                                    <dt>Date</dt>
                                    <dd>{{$date}}</dd>
                                </dl>
                                <dl class="dl-horizontal">
                                    <dt>Time</dt>
                                    <dd>{{$time}}</dd>
                                </dl>
                            </div>
                        </div>
                    </div>
                </div>

                <ul class="tvb-stats">
                    <li class="tvbs-comments">{{DB::table('schedules')->where(['user_id'=>$user->id, 'completed'=>2])->count()}} Completed Tasks</li>
                    <li class="tvbs-likes">{{$user->reviews->count()}} Reviews</li>
                </ul>

                <a class="tvc-more" href=""><i class="zmdi zmdi-mode-comment"></i> View all 54
                    Comments</a>
            </div>

            <div class="tv-comments">
                <ul class="tvc-lists">
                    @forelse($reviews->sortBy('pivot_created_at') as $review)
                        <li class="media">
                            <a href="" class="tvh-user pull-left">
                                <img class="img-responsive" src="{{URL::to('profile/'.$review->user->image)}}" alt="">
                            </a>
                            <div class="media-body">
                                <strong class="d-block">{{$review->user->name}}</strong>
                                <small class="c-gray">{{$review->pivot->created_at->toDayDateTimeString()}}</small>

                                <div class="m-t-10">{{$review->pivot->review}}
                                </div>

                            </div>
                        </li>
                    @empty
                        <li class="media">
                            <div class="media-body">
                                <div class="m-t-10" style="color: #dd7b0b">No reviews for {{$user->name}} at the moment</div>
                            </div>
                        </li>
                    @endforelse
                    @if($showPostButton==null)
                        @if($schedule->status==1 && $schedule->completed==1 || $schedule->completed==2)
                        <form action="{{route('tasker.review')}}" method="POST">
                            {{csrf_field()}}
                            <li class="p-20">
                                <div class="fg-line">
                                    <input type="hidden" name="user_id" value="{{$user->id}}">
                                    <input type="hidden" name="schedule_id" value="{{$schedule_id}}">
                                    <textarea class="form-control auto-size" placeholder="Write a review..." name="review"></textarea>
                                </div>

                                <button class="m-t-15 btn btn-primary btn-sm" type="submit">Post</button>
                            </li>
                        </form>
                        @endif
                    @endif
                </ul>
            </div>
        </div>
        <form style="visibility: hidden" method="POST" action="{{route('tasker.acceptbooking', $user->id)}}" id="{{$user->id}}">
            {{csrf_field()}}
            <input type="hidden" name="time" value="{{$time}}">
            <input type="hidden" name="date" value="{{$date}}">
        </form>
        <form style="visibility: hidden" method="POST" action="{{route('tasker.markcompleted', $user->id)}}" id="{{$user->email}}">
            {{csrf_field()}}
            <input type="hidden" name="time" value="{{$time}}">
            <input type="hidden" name="date" value="{{$date}}">
        </form>
        <form style="visibility: hidden" method="POST" action="{{route('tasker.rejectHiring', $user->id)}}" id="{{$user->username}}">
            {{csrf_field()}}
            <input type="hidden" name="time" value="{{$time}}">
            <input type="hidden" name="date" value="{{$date}}">
            <input type="hidden" name="schedule_id" value="{{$schedule_id}}">
        </form>

        <div class="clearfix"></div>
        <div class=" row text-center" >
            @if($query->status===null)
                <button type="submit" onclick="return submitForm('{{$user->id}}')"
                                class="btn btn-primary  btn-sm">Accept Hiring Request
                </button>
                <button type="submit" onclick="return rejectHiring('{{$user->username}}')"
                        class="btn btn-warning  btn-sm">Reject Hiring Request
                </button>
            @elseif($query->status==2)
                <button type="submit" onclick="return submitForm('{{$user->id}}')"
                        class="btn btn-primary  btn-sm">Accept Hiring Request
                </button>
            @elseif($query->status==1)
                @if($query->completed==2||$query->completed==1)
                    <a href="{{route('tasker.viewclients')}}"><button type="button" class="btn btn-success">OK</button></a>
                @else
                    <button type="submit" onclick="return rejectHiring('{{$user->username}}')"
                            class="btn btn-warning  btn-sm">Reject Hiring Request
                    </button>
                @endif
            @elseif($query->status==0)
                <button type="submit"
                        class="btn btn-warning  btn-sm">{{$user->name}} Canceled this Schedule
                </button>

            @endif
            @if($query->completed==null && $query->status==1)
             <button type="submit" onclick="return markCompleted('{{$user->email}}')" class="btn btn-primary  btn-sm">Mark As Completed </button>
            @endif
        </div>


    </div>

@endsection
@section('scripts')
<script type="text/javascript">
    function submitForm($id) {
        document.getElementById($id).submit()
    }
    function markCompleted($id) {
       document.getElementById($id).submit();

    }
    function  rejectHiring($id) {
        document.getElementById($id).submit();
    }
</script>
@endsection