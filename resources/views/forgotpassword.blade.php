<!DOCTYPE html>
<html lang="en">
<head>
    <title>Kawindi Errands Information System</title>
    <!-- meta tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //meta tags -->
    <!-- custom Theme files -->
    <link href="{{URL::to('css/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{URL::to('css/style2.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link href="{{URL::to('css/chocolat.css')}}" rel="stylesheet">

    <link href="{{URL::to('vendors/bower_components/animate.css/animate.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/sweetalert/dist/sweetalert.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css')}}" rel="stylesheet">

    <link href="{{URL::to('css/app_2.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('css/profileview.min.css')}}" rel="stylesheet">
    <link href="{{URL::to('vendors/summernote/dist/summernote.css')}}" rel="stylesheet">
    <link href="{{URL::to('summernote/dist/summernote.css')}}" rel="stylesheet">
    <script src="{{URL::to('js/jquery-1.11.1.min.js')}}"></script>

    <!-- //js -->

</head>
<body >
<!-- header -->
@include('frontend.includes.header')
@include('frontend.includes.navigation')

{{--@include('frontend.includes.banner')--}}
<div class="container-fluid"style="background-color: #F7F7F7">
    <br><br><br>

    <div class="col-md-8 col-md-offset-2">
        <div class="card">
            <form class="form-horizontal " role="form" method="POST" action="{{route('password.getemail')}}">
                {{csrf_field()}}
                <div class="card-header">
                    <h1>Reset Password
                    </h1>
                </div>
                @if (!empty($status))
                    <div class="alert alert-success">
                        {{ $status }}
                    </div>
                @endif
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                @if (session('warning'))
                    <div class="alert alert-warning">
                        {{ session('warning') }}
                    </div>
                @endif
                @if (!empty($warning))
                    <div class="alert alert-success">
                        {{ $warning }}
                    </div>
                @endif
                <div class="card-body card-padding" >
                    <div class="form-group">
                        <label for="inputEmail3" class="col-md-2 control-label">Email</label>
                        <div class="col-md-6">
                            <div class="fg-line">
                                <input type="email" class="form-control input-sm {{ $errors->has('email') ? ' has-error' : '' }}" id="inputEmail3"
                                       placeholder="Email Address" name="email" required value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" style="color: #dc3545">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" class="btn btn-primary btn-sm">Send Password Reset Link</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<br><br><br><br><br><br>
@include('frontend.includes.footer')
<!-- //header -->

<script src="{{URL::to('')}}"></script>
<!-- start-smooth-scrolling -->
<script type="text/javascript" src="{{URL::to('js/move-top.js')}}"></script>
<script type="text/javascript" src="{{URL::to('js/easing.js')}}"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            // event.preventDefault();

            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- //end-smooth-scrolling -->
<!-- smooth-scrolling-of-move-up -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
         var defaults = {
         containerID: 'toTop', // fading element id
         containerHoverID: 'toTopHover', // fading element hover id
         scrollSpeed: 1200,
         easingType: 'linear'
         };
         */

        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<!-- Placed at the end of the document so the pages load faster -->
<script src="{{URL::to('js/bootstrap.js')}}"></script>
<script src="{{URL::to('js/custom.min.js')}}"></script>
</body>
</html>